	component NIOS_II_COPROCESSOR_MINIMIND is
		port (
			clk_clk : in std_logic := 'X'  -- clk
		);
	end component NIOS_II_COPROCESSOR_MINIMIND;

	u0 : component NIOS_II_COPROCESSOR_MINIMIND
		port map (
			clk_clk => CONNECTED_TO_clk_clk  -- clk.clk
		);

