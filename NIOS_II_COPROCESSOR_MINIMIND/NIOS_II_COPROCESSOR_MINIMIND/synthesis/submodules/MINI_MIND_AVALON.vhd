library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.std_logic_unsigned.all;

entity MINI_MIND_AVALON  is 
	port( clock,resetn 					: in std_logic;
			D				 					: in std_logic_vector(31 downto 0);
			local_byteenable_read 		: in std_logic_vector(3 downto 0);
			local_byteenable_write 		: in std_logic_vector(3 downto 0);
			Q				 					: out std_logic_vector(31 downto 0));
end MINI_MIND_AVALON ;

architecture behavior of MINI_MIND_AVALON is 

	-- MINI MIND 
	component MINI_MIND port(
		clk_MINI_MIND 	 					: IN STD_LOGIC  ;
		reset_MINI_MIND					: IN STD_LOGIC  ;
		enable_MINI_MIND 	 				: IN STD_LOGIC  ;
		--NIO II
		input_nios_bus_data 		  		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		address_nios_ii 		 	  		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		address_write  	 				: IN STD_LOGIC  ;
		address_NIOSII				  		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		result_OUTPUT_NIOSII      		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		--write memory W
		enable_write_memory				: IN STD_LOGIC  ;
		write_memory_select				: IN STD_LOGIC_VECTOR (27	DOWNTO 0);
		dataa_memory_W  					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_W  				: IN STD_LOGIC_VECTOR (13 DOWNTO 0));
	end component;

	signal delay_2  							: std_logic_vector(3 downto 0);
	signal delay_3  							: std_logic_vector(1 downto 0);
	signal local_1_1  						: std_logic;
	signal local_1_2  						: std_logic;
	signal D_1 									: STD_LOGIC_VECTOR (6 DOWNTO 0):="1111111";
	signal Q_1 									: STD_LOGIC_VECTOR (15 DOWNTO 0):=(OTHERS => '0');
	signal Q_2 									: STD_LOGIC_VECTOR (31 DOWNTO 0):=(OTHERS => '0');
	signal bus_write_enable				   : std_logic_vector(27 downto 0);
	signal bus_write_enable_1			   : std_logic_vector(27 downto 0);
	signal delay_5  						   : std_logic_vector(3 downto 0);
	signal delay_4  						   : std_logic:='1';
	signal delay_1  						   : std_logic;
	signal address_w 							: STD_LOGIC_VECTOR (13 DOWNTO 0)  :="11111111111111";	
	signal select_memory_unit_mini_mind : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');		
	signal select_mode_1 					: STD_LOGIC_VECTOR (2 DOWNTO 0)  :=(OTHERS => '0');		
	signal enable_MINI_MIND					: STD_LOGIC;
	signal input_nios_bus_data 			: sTD_LOGIC_VECTOR (15 DOWNTO 0);
	signal address_nios_ii 					: STD_LOGIC_VECTOR (6 DOWNTO 0);
	signal address_write  	 				: STD_LOGIC  ;
	signal reset_MINI_MIND					: STD_LOGIC  ;
	signal enable_write_weights			: STD_LOGIC  ;
	signal data_memory_weight				: STD_LOGIC_VECTOR (15 DOWNTO 0);	

	
begin 

	mini_mind_1 : MINI_MIND     port map(
						clk_MINI_MIND							=> clock,
						reset_MINI_MIND						=> reset_MINI_MIND,
						enable_MINI_MIND 						=> enable_MINI_MIND,
						input_nios_bus_data 					=>input_nios_bus_data,
						address_nios_ii						=>address_nios_ii,
						address_write  	 					=>address_write,
						address_NIOSII							=>D_1,
						result_OUTPUT_NIOSII					=>Q_1,
						--write memory W
						enable_write_memory					=>enable_write_weights,
						write_memory_select					=>bus_write_enable_1,
						dataa_memory_W  						=>data_memory_weight,
						address_memory_W  					=>address_w);

						
	---address_weights convert to a bus data 
	process (clock)
	begin 
	
		if(clock'event and clock ='1') then	
				select_memory_unit_mini_mind  <= D(30 downto 26);
				if local_1_1= '0' then									
						bus_write_enable 					<= (OTHERS => '0');
						select_memory_unit_mini_mind  <= (OTHERS => '0');
				else 
						case select_memory_unit_mini_mind is
								when "00001" =>  
								bus_write_enable<="0000000000000000000000000001"; 
								when "00010" =>  
								bus_write_enable<="0000000000000000000000000010"; 
								when "00011" =>  
								bus_write_enable<="0000000000000000000000000100"; 
								when "00100" =>  
								bus_write_enable<="0000000000000000000000001000"; 
								when "00101" =>  
								bus_write_enable<="0000000000000000000000010000"; 
								when "00110" =>  
								bus_write_enable<="0000000000000000000000100000"; 
								when "00111" =>  
								bus_write_enable<="0000000000000000000001000000"; 
								when "01000" =>  
								bus_write_enable<="0000000000000000000010000000"; 
								when "01001" =>  
								bus_write_enable<="0000000000000000000100000000"; 
								when "01010" =>  
								bus_write_enable<="0000000000000000001000000000"; 
								when "01011" =>  
								bus_write_enable<="0000000000000000010000000000"; 
								when "01100" =>  
								bus_write_enable<="0000000000000000100000000000"; 
								when "01101" =>  
								bus_write_enable<="0000000000000001000000000000";
								when "01110" =>  
								bus_write_enable<="0000000000000010000000000000";							
								when "01111" =>  
								bus_write_enable<="0000000000000100000000000000";
								when "10000" =>  
								bus_write_enable<="0000000000001000000000000000";
								when "10001" =>  
								bus_write_enable<="0000000000010000000000000000";
								when "10010" =>  
								bus_write_enable<="0000000000100000000000000000";
								when "10011" =>  
								bus_write_enable<="0000000001000000000000000000";
								when "10100" =>  
								bus_write_enable<="0000000010000000000000000000";
								when "10101" =>  
								bus_write_enable<="0000000100000000000000000000";
								when "10110" =>  
								bus_write_enable<="0000001000000000000000000000";
								when "10111" =>  
								bus_write_enable<="0000010000000000000000000000";
								when "11000" =>  
								bus_write_enable<="0000100000000000000000000000";
								when "11001" =>  
								bus_write_enable<="0001000000000000000000000000";
								when "11010" =>  
								bus_write_enable<="0010000000000000000000000000";
								when "11011" =>  
								bus_write_enable<="0100000000000000000000000000";								
								when "11100" =>  
								bus_write_enable<="1000000000000000000000000000";								
								when others => 
								bus_write_enable<="0000000000000000000000000000";
			
						end case;
				end if;
		end if;
	end process;


	local_1_2 <= local_byteenable_read(0) and local_byteenable_read(1) and local_byteenable_read(2) and local_byteenable_read(3);

	process (clock)
	begin 
	
		if(clock'event and clock ='1') then	
					if reset_MINI_MIND = '1' then	
							delay_1 <= '0';
							D_1 <= "1111111";
							delay_2 <= (OTHERS => '0');
					end if; 
					if local_1_2 = '1' then	
			
							if delay_1= '0' then	
									D_1 <= D_1 + '1';	
									delay_1 <= '1';
							end if;
							if delay_2= "0100" then
									delay_1 <= '0';
									delay_2 <= "0000";
									Q_2(15 downto 0) <= Q_1;
									Q <= Q_2;							
							else 	
									delay_2 <= delay_2 +'1';
							
							end if;
					end if;
	
		end if;
	end process;
	
	local_1_1 <= local_byteenable_write(0) and local_byteenable_write(1) and local_byteenable_write(2) and local_byteenable_write(3);
	
	process (clock)
	begin 
		if(clock'event and clock ='1') then	
				select_mode_1 	<= D(25 downto 23);
				if local_1_1= '0' then									
								address_write <='0';
								input_nios_bus_data <= (OTHERS => '0');
								address_nios_ii 	  <= (OTHERS => '0');
								select_mode_1		  <= (OTHERS => '0');
								bus_write_enable_1  <= (OTHERS => '0');
								enable_write_weights <= '0';
								delay_4 <= '1';
								data_memory_weight  <= (OTHERS => '0');
						
				else 			
								if select_mode_1= "001" then							--write in input_buffer_memory
											enable_MINI_MIND <= '0'; 
											reset_MINI_MIND <= '0';	
											address_write <='1'; 
											address_nios_ii <= D(22 downto 16);
											input_nios_bus_data <= D(15 downto 0);
								end if;
								if select_mode_1= "010" then							--no write in input_buffer_memory
											reset_MINI_MIND <= '0';		
											enable_MINI_MIND <= '0';
											address_write <='0';
								end if;								
								if select_mode_1= "011" then							--reset all registers and counters -- MINI_SYSTEM
											enable_MINI_MIND <= '0'; 
											reset_MINI_MIND <= '1';
											input_nios_bus_data <= (OTHERS => '0');
											address_write <='0';
											delay_3<= (OTHERS => '0');
								end if;
							
								if select_mode_1= "100" then							--enable_MINI_MIND
											reset_MINI_MIND <= '0';
											address_write <='0'; 
											if delay_3 = "11" then
													enable_MINI_MIND <= '1';    
											else
													address_nios_ii <= "1111111";
													delay_3 <= delay_3 + 1;
											end if;
								end if;			
								if select_mode_1= "110" then							--write internal memory - weights 
											if delay_4= '1' then	
													address_w <= address_w + '1';	
													data_memory_weight <= D(15 downto 0);
													delay_4 <= '0';
											else
													enable_write_weights <= '1';
													bus_write_enable_1 <= bus_write_enable;
										
											end if;
								
								end if;			
		
								if select_mode_1= "111" then							--reset -- counter memory unit - 
											address_w <= "11111111111111";
										
								
								end if;	

				end if;
		end if;
	end process;	

end behavior;						
	
