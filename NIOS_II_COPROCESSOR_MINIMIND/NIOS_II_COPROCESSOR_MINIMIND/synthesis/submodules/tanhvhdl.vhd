LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;



entity tanhvhdl is               
  port(
	   clk_tanh  	      : IN STD_LOGIC  ;
		reset_tanh	      : IN STD_LOGIC  ;
		enable_tanh	   	: IN STD_LOGIC  ;
		input_tanh  		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		extra_logic_result : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		result_1          : OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end tanhvhdl;

architecture Behavioral of tanhvhdl is 

--Mac component 16-signed
--Logic comparator
component logic_comparator_tanh is               
  port(clk_comparator: IN STD_LOGIC  ;
		 dataa_input	: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		 result_1    	: OUT STD_LOGIC_VECTOR (3 DOWNTO 0));
end component;

--Logic adder
component adder_1 port
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result	: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;


component multionly port (
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result	: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
end component;


signal logic_result : STD_LOGiC_VECTOR (3 DOWNTO 0):=(OTHERS => '0');
signal aux_a_line	  : STD_LOGIC_VECTOR (15 DOWNTO 0):=(OTHERS => '0');
signal aux_b_line	  : STD_LOGIC_VECTOR (15 DOWNTO 0):=(OTHERS => '0');
signal result_multi : STD_LOGIC_VECTOR (31 DOWNTO 0):=(OTHERS => '0');
signal result_multi_3 : STD_LOGIC_VECTOR (15 DOWNTO 0):=(OTHERS => '0');


--signal result_1_vector : STD_LOGIC_VECTOR (15 DOWNTO 0):=(OTHERS => '0');



begin


unitcomparator: logic_comparator_tanh port map (
									clk_comparator=> clk_tanh,
									dataa_input	  => input_tanh,
									result_1      => logic_result);

extra_logic_result <= logic_result;
									
unit_tanh:  multionly  port map(

									clock  => clk_tanh,
									dataa	 => input_tanh,
									datab	 => aux_a_line,
									result => result_multi);
									
result_multi_3(14 DOWNTO 0)<=result_multi(22 DOWNTO 8);
result_multi_3(15)<=result_multi(31);

unitadder: adder_1 port map(
									clock		=>clk_tanh,
									dataa		=>result_multi_3,
									datab		=>aux_b_line,
									result	=>result_1);
									

									
process (clk_tanh,logic_result)
begin  
if(clk_tanh'event and clk_tanh='1') then	
	if(reset_tanh ='1') then	
				aux_a_line<="0000000000000000"; 
				aux_b_line<="0000000000000000";	
	end if;

	if(enable_tanh ='1') then	
		case logic_result is
				when "0001" =>  
				aux_a_line<="0000000000000000"; --  0
				aux_b_line<="1111111100000000"; -- -1
				
				when "0010" =>  
				aux_a_line<="0000000000000000"; -- 0.000213386
				aux_b_line<="1111111100000001"; -- -0.998792
				
				when "0011" =>  
				aux_a_line<="0000000000000000"; -- 0.001575
				aux_b_line<="1111111100000010"; -- -0.9926643
				
				when "0100" =>  
				aux_a_line<="0000000000000010"; -- 0.0115636
				aux_b_line<="1111111100001011"; -- -0.9577		
		
				when "0101" =>  
				aux_a_line<="0000000000010100"; -- 0.0814666
				aux_b_line<="1111111100111000"; -- -0.782949	
				
				when "0110" =>  
				aux_a_line<="0000000001110001"; -- 0.44303
				aux_b_line<="1111111111000011"; -- -0.2406	
				
				when "0111" =>  
				aux_a_line<="0000000011101100"; -- 0.92423
				aux_b_line<="0000000000000000"; -- 0
				
				when "1000" =>  
				aux_a_line<="0000000001110001"; -- 0.443031
				aux_b_line<="0000000000111101"; -- 0.2406

				when "1001" =>  
				aux_a_line<="0000000000010100"; --0.08146
				aux_b_line<="0000000011001000"; --0.782949

				when "1010" =>  
				aux_a_line<="0000000000000011"; --0.01156
				aux_b_line<="0000000011110101"; --0.9577053
				
				when "1011" =>  
				aux_a_line<="0000000000000000"; --0
				aux_b_line<="0000000011111110"; --0.99266

				when "1100" =>  
				aux_a_line<="0000000000000000"; --0
				aux_b_line<="0000000011111111"; --0.9987
				
				when "1101" =>  
				aux_a_line<="0000000000000000"; --0
				aux_b_line<="0000000100000000"; --1
				
				when others => 
				aux_a_line<="0000000000000000"; 
				aux_b_line<="0000000000000000";
		end case;		
	end if;
end if;
end process;

end Behavioral;