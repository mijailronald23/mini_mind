library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

entity MEMORY_CELL_1 is               
  port(
		clk_MEMORY_CELL_1 	 						: IN STD_LOGIC  ;
		reset_MEMORY_CELL_1  		 	 			: IN STD_LOGIC  ;
		enable_MEMORY_CELL_1 						: IN STD_LOGIC  ;
		enable_WRITE_MEMORY_CELL_1 				: IN STD_LOGIC  ;
		data_INPUT_MEMORY_CELL_1 	 				: IN STD_LOGIC_VECTOR (15	DOWNTO 0);		
		result_MEMORY_CELL_1 				     	: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end MEMORY_CELL_1;


architecture Behavioral of MEMORY_CELL_1 is 

---RAM (1X128) -- MEMORY-1
component UNIT_1_MEMORY_CELL_1 port(
		aclr		: IN STD_LOGIC  := '0';
		address		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		clock			: IN STD_LOGIC  ;
		data			: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		rden			: IN STD_LOGIC  ;
		wren			: IN STD_LOGIC ;
		q				: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;

---RAM (1X128) -- MEMORY-2
component UNIT_1_MEMORY_CELL_2 port(
		aclr		: IN STD_LOGIC  := '0';
		address		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		clock		: IN STD_LOGIC  ;
		data		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		rden		: IN STD_LOGIC  ;
		wren		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;


---MUX 
component UNIT_1_MUX port(
		clock			: IN STD_LOGIC ;
		data0x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data1x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		sel			: IN STD_LOGIC ;
		result		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;


signal address_H_1								 : STD_LOGIC_VECTOR (6 DOWNTO 0)	 :="1111111";
signal address_H_2								 : STD_LOGIC_VECTOR (6 DOWNTO 0)	 :="1111111";
signal counter_reset_1							 : STD_LOGIC_VECTOR (7 DOWNTO 0)	 :=(OTHERS => '0');
signal read_enable_1			   				 : STD_LOGIC  :='0';
signal read_enable_2			   				 : STD_LOGIC  :='0';
signal write_enable_1		   				 : STD_LOGIC  :='0';
signal write_enable_2		   				 : STD_LOGIC  :='0';
signal memory_output_1_cell					 : STD_LOGIC_VECTOR (15 DOWNTO 0)	 :=(OTHERS => '0');
signal memory_output_2_cell					 : STD_LOGIC_VECTOR (15 DOWNTO 0)	 :=(OTHERS => '0');
signal enable_mux				   				 : STD_LOGIC  							 :='0';
signal counter_1_1		 						 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_1_2		 						 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal extra_1		 								 : STD_LOGIC_VECTOR (2 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_1_1 		 						 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_1_2 		 						 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal counter_address_1						 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_address_2						 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_address_3						 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_address_4						 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal extra_2		 								 : STD_LOGIC_VECTOR (2 DOWNTO 0)  :=(OTHERS => '0');
signal signal_control_section_unit_1		 : STD_LOGIC:= '0';
signal signal_control_section_unit_2		 : STD_LOGIC:='0';
signal counter_1mux		 						 : STD_LOGIC_VECTOR (7 DOWNTO 0)	 :=(OTHERS => '0');
signal extra_1mux		 							 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_address_1mux					 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_1_mux 		 					 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal counter_2mux		 						 : STD_LOGIC_VECTOR (7 DOWNTO 0)	 :=(OTHERS => '0');
signal extra_2mux		 							 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_address_2mux					 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_2_mux 		 					 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal signal_control_mux_unit_1				 : STD_LOGIC:= '0';
signal data_data_INPUT_MEMORY_CELL_1_init  : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal signal_reset_memory  					 : STD_LOGIC;
signal signal_reset_memory_2  				 : STD_LOGIC;
signal delay_reset  								 : STD_LOGIC_VECTOR (1 DOWNTO 0)  :=(OTHERS => '0');

signal delay_Ht_1_3 		 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_1_4 		 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');


begin



UNIT_MEMORY_CELL_1 : UNIT_1_MEMORY_CELL_1 port map(
							aclr			=>reset_MEMORY_CELL_1,
							address		=>address_H_1,
							clock			=>clk_MEMORY_CELL_1,
							data			=>data_data_INPUT_MEMORY_CELL_1_init,
							rden			=>read_enable_1,
							wren			=>write_enable_1,
							q				=>memory_output_1_cell);
							

UNIT_MEMORY_CELL_2 : UNIT_1_MEMORY_CELL_2 port map(
							aclr			=>reset_MEMORY_CELL_1,
							address		=>address_H_2,
							clock			=>clk_MEMORY_CELL_1,
							data			=>data_INPUT_MEMORY_CELL_1,
							rden			=>read_enable_2,
							wren			=>write_enable_2,
							q				=>memory_output_2_cell);


UNIT_MUX_SELECT	: UNIT_1_MUX port	map(
							clock			=>clk_MEMORY_CELL_1,
							data0x		=>memory_output_1_cell,
							data1x		=>memory_output_2_cell,
							sel			=>enable_mux,
							result		=>result_MEMORY_CELL_1);

							
process (clk_MEMORY_CELL_1)
begin  
if(clk_MEMORY_CELL_1 'event and clk_MEMORY_CELL_1 ='1') then
		 if (reset_MEMORY_CELL_1 = '1') then
				data_data_INPUT_MEMORY_CELL_1_init <= (OTHERS => '0');
		 end if;
		 if (reset_MEMORY_CELL_1 = '0') then
				data_data_INPUT_MEMORY_CELL_1_init <= data_INPUT_MEMORY_CELL_1;
		 end if;

end if;	
end process;						
-----SIGNAL-READY
process (clk_MEMORY_CELL_1,enable_MEMORY_CELL_1)
begin  
if(clk_MEMORY_CELL_1 'event and clk_MEMORY_CELL_1 ='1') then
		if (reset_MEMORY_CELL_1 = '1') then
				delay_Ht_2_mux <= (OTHERS => '0');
				signal_control_mux_unit_1 <= '0';		
				counter_2mux <=(OTHERS => '0');
				counter_address_1mux <=(OTHERS => '0');
				extra_2mux  <= (OTHERS => '0');							
				counter_1mux <=(OTHERS => '0');
				extra_1mux  <= (OTHERS => '0');			
				delay_Ht_1_mux <= (OTHERS => '0');
				counter_address_2mux <=(OTHERS => '0');
				enable_mux <= '0';
		end if;
		if (enable_MEMORY_CELL_1 = '1') then
			if (signal_control_mux_unit_1 = '0') then
							if ( counter_1mux > "01111111") then  --128
											if ( extra_1mux= "100") then  --DELAY CLEAR MAC
													counter_1mux <=(OTHERS => '0');
													extra_1mux  <= (OTHERS => '0');
													counter_address_1mux <= counter_address_1mux+1;
													enable_mux <= '0';
											else		
													extra_1mux <= extra_1mux + 1;
											end if;
							else	
											counter_1mux <= counter_1mux +1;
											
							end if;
			
				
							if ( counter_address_1mux > "01111111") then
											counter_1mux <=(OTHERS => '0');
											counter_address_2mux <=(OTHERS => '0');
											extra_1mux  <= (OTHERS => '0');
											
											if (delay_Ht_1_mux > "11000") then
													counter_address_1mux <= (OTHERS => '0');
													delay_Ht_1_mux <= (OTHERS => '0');
													signal_control_mux_unit_1 <= '1';
													
							
											else 
													delay_Ht_1_mux <= delay_Ht_1_mux +1;

											end if;
						  end if;
							
			end if;
			if (signal_control_mux_unit_1 = '1') then
							
							if ( counter_2mux > "01111111") then  --128
											if ( extra_2mux= "100") then  --DELAY CLEAR MAC
													counter_2mux <=(OTHERS => '0');
													extra_2mux  <= (OTHERS => '0');
													counter_address_2mux <= counter_address_2mux+1;
													enable_mux <= '1';
											else		
													extra_2mux <= extra_2mux + 1;
											end if;
							else	
											counter_2mux <= counter_2mux +1;
											
							end if;
			
				
							if ( counter_address_2mux > "01111111") then
											counter_2mux <=(OTHERS => '0');
											counter_address_1mux <=(OTHERS => '0');
											extra_2mux  <= (OTHERS => '0');
											if (delay_Ht_2_mux > "11000") then
													counter_address_1mux <= (OTHERS => '0');
													delay_Ht_2_mux <= (OTHERS => '0');
													signal_control_mux_unit_1 <= '0';
													
							
											else 
													delay_Ht_2_mux <= delay_Ht_2_mux +1;

											end if;
						  end if;
			 end if;		
			
		end if;
				

end if;
end process;

---MEMORY-1-CONFIGURATION-FOR READ AND WRITE				
process (clk_MEMORY_CELL_1,enable_MEMORY_CELL_1)
begin  
if(clk_MEMORY_CELL_1 'event and clk_MEMORY_CELL_1 ='1') then

	  if (reset_MEMORY_CELL_1 = '1') then
			counter_1_1 <=(OTHERS => '0');
			extra_1  <= (OTHERS => '0');	  						
			counter_address_1 <= (OTHERS => '0');
			delay_Ht_1_1 <= (OTHERS => '0');
			signal_control_section_unit_1 <= '0';	
			read_enable_1  <= '0';									
			counter_address_2 <=(OTHERS => '0');			
			if (signal_reset_memory = '1') then		
					write_enable_1 <= '1';
					if (signal_reset_memory_2 = '1') then
							address_H_1 <= "1111111";
							signal_reset_memory_2 <= '0';
	
					else	
							if (delay_reset = "11") then		 	 			
										address_H_1 <= address_H_1+1;
										counter_reset_1 <= counter_reset_1+1;
										delay_reset <= (OTHERS => '0');
										
							else 
										delay_reset <= delay_reset+ '1';
							end if;
							if (counter_reset_1 > "10000000") then	
										address_H_1 <= "1111111";							
										signal_reset_memory <= '0';
										write_enable_1 <= '0';	
							end if;	
				   end if;
			end if;	
			  
	  end if;

	  if (enable_MEMORY_CELL_1 = '1') then
	  		signal_reset_memory <= '1';
			signal_reset_memory_2 <= '1';
			delay_reset <= (OTHERS => '0');
			counter_reset_1<= (OTHERS => '0');
			---READ-MEMORY-1				
			--if (signal_control_section_unit < "10000000") then						--COUNTER-128-UNITS	
			if (signal_control_section_unit_1 = '0') then

																	--ENABLE OUTPUT_1 -- MEMORY_1
							
							if ( counter_1_1 > "01111111") then  --128
											if ( extra_1= "100") then  --DELAY CLEAR MAC
													counter_1_1 <=(OTHERS => '0');
													extra_1  <= (OTHERS => '0');
													counter_address_1 <= counter_address_1+1;
													address_H_1 <= address_H_1 + 1;
													write_enable_1 <= '0';										--NO WRITE MEMORY_1
													read_enable_1  <= '1';										--READ MEMORY_1
													--enable_mux     <= '0';
											else		
													extra_1 <= extra_1 + 1;
											end if;
							else	
											counter_1_1 <= counter_1_1 +1;
											
							end if;
			
				
							if ( counter_address_1 > "01111111") then
											counter_1_1 <=(OTHERS => '0');
											counter_address_2 <=(OTHERS => '0');
											extra_1  <= (OTHERS => '0');
											address_H_1 <= "1111111";
																						--ENABLE OUTPUT_2 -- MEMORY_1
											read_enable_1  <= '0';
											if (delay_Ht_1_1 > "11000") then
													counter_address_1 <= (OTHERS => '0');
													delay_Ht_1_1 <= (OTHERS => '0');
													signal_control_section_unit_1 <= '1';
													
							
											else 
													delay_Ht_1_1 <= delay_Ht_1_1 +1;

											end if;
						  end if;
							
			end if;
		
		
			---WRITE-MEMORY-1					
			--if (counter_128_128_memory_1 >= "10000000") then						--COUNTER-128-UNITS
			if (signal_control_section_unit_1 = '1') then
						
						read_enable_1  <= '0';
						if (enable_WRITE_MEMORY_CELL_1 = '1') then	
									address_H_1 <= address_H_1 +1;
									write_enable_1 <= '1';	
									--enable_mux     <= '1';									--NO WRITE MEMORY_1
									counter_address_2 <= counter_address_2+1;
						end if;
						if (enable_WRITE_MEMORY_CELL_1 = '0') then	
									write_enable_1 <= '0';											--NO WRITE MEMORY_1
																	
						end if;
						
						
						if ( counter_address_2 > "01111111") then
											--if (delay_Ht_1_2 > "10111") then
									address_H_1 <= "1111111";
									counter_address_1 <= (OTHERS => '0');
									counter_1_1 <=(OTHERS => '0');
									extra_1  <= (OTHERS => '0');
									delay_Ht_1_1 <= (OTHERS => '0');
									--delay_Ht_1_2 <= (OTHERS => '0');
									signal_control_section_unit_1 <= '0';
									write_enable_1 <= '0';
									read_enable_1  <= '0';
											--else 
											--		delay_Ht_1_2 <= delay_Ht_1_2 +1;

											--end if;
						end if;
							
						
			end if;

	  end if;
end if;
end process;

---MEMORY-2-CONFIGURATION-FOR READ AND WRITE				
process (clk_MEMORY_CELL_1,enable_MEMORY_CELL_1)
begin  
if(clk_MEMORY_CELL_1 'event and clk_MEMORY_CELL_1 ='1') then

	  if (reset_MEMORY_CELL_1 = '1') then
			counter_1_2 <=(OTHERS => '0');
			extra_2  <= (OTHERS => '0');
			address_H_2 <= "1111111";
			counter_address_3 <= (OTHERS => '0');
			delay_Ht_1_3 <= (OTHERS => '0');
			signal_control_section_unit_2 <= '0';
			write_enable_2 <= '0';										--NO WRITE MEMORY_1
			read_enable_2  <= '0';										--READ MEMORY_1	  
			counter_address_4 <= (OTHERS => '0');
			delay_Ht_1_4 <= (OTHERS => '0');
	
	  end if;
	  if (enable_MEMORY_CELL_1 = '1') then
				
			---WRITE-MEMORY-2			
			--if (counter_128_128_memory_2 < "10000000") then								--COUNTER-128-UNITS
			if (signal_control_section_unit_2 = '0') then
						read_enable_2  <= '0';									
						if (enable_WRITE_MEMORY_CELL_1 = '1') then	
									write_enable_2 <= '1';			
									counter_address_3 <= counter_address_3+1;
									address_H_2 <= address_H_2 +1;
								

									
						end if;
						if (enable_WRITE_MEMORY_CELL_1 = '0') then	
									write_enable_2 <= '0';											--NO WRITE MEMORY_2	
									
						end if;
						if ( counter_address_3 > "01111111") then
											--if (delay_Ht_1_3 > "00001") then
									counter_1_2 <=(OTHERS => '0');
									extra_2  <= (OTHERS => '0');
									address_H_2 <= "1111111";
									counter_address_3 <= (OTHERS => '0');
									delay_Ht_1_3 <= (OTHERS => '0');
									signal_control_section_unit_2 <= '1';
									write_enable_2 <= '0';										--NO WRITE MEMORY_1
									read_enable_2  <= '0';										--READ MEMORY_1
							
											--else 
													--delay_Ht_1_3 <= delay_Ht_1_3 +1;

											--end if;
						end if;	
					
						
						
			end if;


			---READ-MEMORY-2				
			--if (counter_128_128_memory_2 >= "10000000") then					--COUNTER-128-UNITS	
			if (signal_control_section_unit_2 = '1') then

							if ( counter_1_2 > "01111111") then  --128
											if ( extra_2= "100") then  --DELAY CLEAR MAC
													counter_1_2 <=(OTHERS => '0');
													extra_2  <= (OTHERS => '0');
													counter_address_4 <= counter_address_4+1;
													write_enable_2 <= '0';										--NO WRITE MEMORY_1
													read_enable_2  <= '1';										--READ MEMORY_1
		
													address_H_2 <= address_H_2+1;
											else		
													extra_2 <= extra_2 + 1;
											end if;
							else	
											counter_1_2 <= counter_1_2 +1;
											
							end if;
			
				
							if ( counter_address_4 > "01111111") then
											counter_1_2 <=(OTHERS => '0');
											extra_2  <= (OTHERS => '0');
											address_H_2 <= "1111111";
											if (delay_Ht_1_4 > "11000") then
													counter_address_4 <= (OTHERS => '0');
													delay_Ht_1_4 <= (OTHERS => '0');
													signal_control_section_unit_2 <= '0';
							
											else 
													delay_Ht_1_4 <= delay_Ht_1_4 +1;

											end if;
						  end if;
							
			end if;
			
	end if ;
end if;
end process;

end Behavioral;				