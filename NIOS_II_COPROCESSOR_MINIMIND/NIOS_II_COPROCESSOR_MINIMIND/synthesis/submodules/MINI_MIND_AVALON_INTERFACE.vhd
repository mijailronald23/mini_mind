library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.std_logic_unsigned.all;

entity MINI_MIND_AVALON_INTERFACE is
	port ( clock,resetn 			  : in std_logic;
			 read_1,write_1,chipselect : in std_logic;
			 writedata				  : in std_logic_vector(31 downto 0);
			 byteenable   			  : in std_logic_vector(3 downto 0);
			 readdata	  			  : out std_logic_vector(31 downto 0));
end MINI_MIND_AVALON_INTERFACE;

architecture structure of MINI_MIND_AVALON_INTERFACE is 
	signal local_byteenable : std_logic_vector(3 downto 0);
	signal local_byteenable_read : std_logic_vector(3 downto 0);
	signal extra_1  : std_logic_vector(31 downto 0);
	signal extra_2  : std_logic;

	component MINI_MIND_AVALON  is 
	port( clock,resetn : in std_logic;
			D				 : in std_logic_vector(31 downto 0);
			local_byteenable_read : in std_logic_vector(3 downto 0);
			local_byteenable_write : in std_logic_vector(3 downto 0);
			Q				 : out std_logic_vector(31 downto 0));
	end component;
	
begin 
	--to_reg <= writedata;
	with (chipselect and write_1) select	
			local_byteenable <= byteenable when '1', "0000" when others;
	with (chipselect and read_1) select	
			local_byteenable_read <= byteenable when '1', "0000" when others;			
			
	mini_mind_1:MINI_MIND_AVALON  port map(clock,resetn,writedata,local_byteenable_read,local_byteenable,extra_1);
	readdata <= extra_1;
	
end structure;