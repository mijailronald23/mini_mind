library IEEE;
use IEEE.std_logic_1164.all;

entity logic_comparator_sigmoid is               
  port(
	   
		clk_comparator	      : IN STD_LOGIC  ;
		--enable_comparator		: IN STD_LOGIC  ;
		dataa_input				: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result_1    			: OUT STD_LOGIC_VECTOR (1 DOWNTO 0));
end logic_comparator_sigmoid;

architecture Behavioral of logic_comparator_sigmoid is 

signal signal_comparator : STD_LOGIC :='0';
signal vector_1 : STD_LOGIC_VECTOR (14 DOWNTO 0):=(OTHERS => '0') ;
signal output_1 : STD_LOGiC_VECTOR (1 DOWNTO 0):=(OTHERS => '0') ;

begin
signal_comparator <= dataa_input(15);

vector_1 <= dataa_input (14 DOWNTO 0);


result_1 <= output_1;
process (clk_comparator)
begin  
if(clk_comparator'event and clk_comparator='1') then
		
		if (signal_comparator = '1') then  --case negatives
						if (vector_1<"111110110000000") then
								output_1 <= "00"; 
						else		
								output_1 <= "01"; 

						end if;		
		end if;
		if (signal_comparator = '0') then  -- case positive 
						if (vector_1>"000001010000000") then
								output_1 <= "10"; 
						else 
								output_1 <= "01"; 
						end if;
			
		end if;
	
		
end if;
end process;

end Behavioral;