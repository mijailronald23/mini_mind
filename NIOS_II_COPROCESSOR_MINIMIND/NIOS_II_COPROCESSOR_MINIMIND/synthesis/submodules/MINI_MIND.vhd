library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

entity MINI_MIND is               
  port(
		clk_MINI_MIND 	 					: IN STD_LOGIC  ;
		reset_MINI_MIND					: IN STD_LOGIC  ;
		enable_MINI_MIND 	 				: IN STD_LOGIC  ;
		--NIO II
		input_nios_bus_data 		  		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		address_nios_ii 		 	  		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		address_write  	 				: IN STD_LOGIC  ;
		address_NIOSII				  		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		result_OUTPUT_NIOSII      		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		--write memory W
		enable_write_memory				: IN STD_LOGIC  ;
		write_memory_select				: IN STD_LOGIC_VECTOR (27	DOWNTO 0);
		dataa_memory_W  					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_W  				: IN STD_LOGIC_VECTOR (13 DOWNTO 0));
		
end MINI_MIND;

architecture Behavioral of MINI_MIND is 


------INPUT BFFER
component INPUT_BUFFER port(
		clk_INPUT_BUFFER 					: IN STD_LOGIC  ;
		reset_INPUT_BUFFER	 	  		: IN STD_LOGIC  ;
		input_nios_bus_data 		  		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		address_nios_ii 		 	  		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		address_write  	 		 		: IN STD_LOGIC  ;
		enable_MINI_MIND 		     		: IN STD_LOGIC  ;	
		result_INPUT_BUFFER       		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;

------LSTM_1
component LSTM_1 port(
		clk_LSTM_1 	 						: IN STD_LOGIC  ;
		reset_LSTM_1					   : IN STD_LOGIC ;
		enable_LSTM_1 						: IN STD_LOGIC  ;		
		data_INPUT_LSTM_Wx				: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		--nios_ii 
		enable_write_memory				: IN STD_LOGIC  ;
		write_memory_select				: IN STD_LOGIC_VECTOR (11	DOWNTO 0);
		dataa_memory_W  					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_W  				: IN STD_LOGIC_VECTOR (13 DOWNTO 0);		 
		-------
		output_LSTM_1	      			: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;

------LSTM_2
component LSTM_2 port(
		clk_LSTM_2 	 						: IN STD_LOGIC  ;
		reset_LSTM_2 		 	 			: IN STD_LOGIC  ;		
		enable_LSTM_1 						: IN STD_LOGIC  ;
		data_INPUT_LSTM_Wx				: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		--nios_ii 
		enable_write_memory				: IN STD_LOGIC  ;
		write_memory_select				: IN STD_LOGIC_VECTOR (11	DOWNTO 0);
		dataa_memory_W  					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_W  				: IN STD_LOGIC_VECTOR (13 DOWNTO 0);		 
		-------
		output_LSTM_1	      			: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;



------FULLY_1
component FULLY_1 port(
		clk_FULLY_1 	 				: IN STD_LOGIC  ;
		reset_FULLY_1 	 				: IN STD_LOGIC  ;
		enable_FULLY_1 				: IN STD_LOGIC  ;
		data_FULLY_1_Wh 				: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		--nios_ii 
		enable_write_memory			: IN STD_LOGIC  ;
		write_memory_select			: IN STD_LOGIC_VECTOR (1	DOWNTO 0);
		dataa_memory_W  				: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_W  			: IN STD_LOGIC_VECTOR (13 DOWNTO 0);		 
		-------
		HT_MEMORY_FULLY_1     		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;

------FULLY_2
component FULLY_2 port(
		clk_FULLY_2 	 					: IN STD_LOGIC  ;
		reset_FULLY_2 	 					: IN STD_LOGIC  ;
		enable_FULLY_2 					: IN STD_LOGIC  ;
		data_FULLY_2_Wh 					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		result_RELU_FULLY_2      		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		extra_output_fully_2		    	: OUT STD_LOGIC;                     
		--nios_ii 
		enable_write_memory				: IN STD_LOGIC  ;
		write_memory_select				: IN STD_LOGIC_VECTOR (1	DOWNTO 0);
		dataa_memory_W  					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_W  				: IN STD_LOGIC_VECTOR (13 DOWNTO 0));
end component;

------OUTPUT FOR NIOSII
component OUTPUT_NIOSII
	port(
		clk_OUTPUT_NIOSII 	 						: IN STD_LOGIC  ;
		reset_OUTPUT_NIOSII 	 						: IN STD_LOGIC  ;
		enable_OUTPUT_NIOSII 						: IN STD_LOGIC  ;
		enable_WRITE_OUTPUT_NIOSII 				: IN STD_LOGIC  ;
		data_INPUT_OUTPUT_NIOSII 	 				: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		counter_signaltap 							: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		address_NIOSII									: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		result_OUTPUT_NIOSII 				     	: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));	
end component;



signal extra_output_MINI_MIND_1 			: STD_LOGIC_VECTOR (15 DOWNTO 0);		
signal signal_enable_LSTM_2 				: STD_LOGIC:='0';	
signal counter_1mux 							: STD_LOGIC_VECTOR(7 DOWNTO 0):=(OTHERS => '0');
signal counter_2mux 							: STD_LOGIC_VECTOR(7 DOWNTO 0):=(OTHERS => '0');	
signal counter_3mux 							: STD_LOGIC_VECTOR(7 DOWNTO 0):=(OTHERS => '0');	
signal counter_4mux 							: STD_LOGIC_VECTOR(7 DOWNTO 0):=(OTHERS => '0');	
signal extra_1mux								: STD_LOGIC_VECTOR(2 DOWNTO 0):=(OTHERS => '0');
signal extra_2mux								: STD_LOGIC_VECTOR(2 DOWNTO 0):=(OTHERS => '0');
signal extra_3mux								: STD_LOGIC_VECTOR(2 DOWNTO 0):=(OTHERS => '0');
signal extra_4mux								: STD_LOGIC_VECTOR(2 DOWNTO 0):=(OTHERS => '0');
signal counter_address_1mux				: STD_LOGIC_VECTOR(7 DOWNTO 0):=(OTHERS => '0');	
signal counter_address_2mux 				: STD_LOGIC_VECTOR(7 DOWNTO 0):=(OTHERS => '0');	
signal counter_address_3mux 				: STD_LOGIC_VECTOR(7 DOWNTO 0):=(OTHERS => '0');	
signal counter_address_4mux 				: STD_LOGIC_VECTOR(7 DOWNTO 0):=(OTHERS => '0');	
signal delay_Ht_1_mux 						: STD_LOGIC_VECTOR(4 DOWNTO 0):=(OTHERS => '0');
signal delay_Ht_2_mux 						: STD_LOGIC_VECTOR(4 DOWNTO 0):=(OTHERS => '0');
signal delay_Ht_3_mux 						: STD_LOGIC_VECTOR(4 DOWNTO 0):=(OTHERS => '0');
signal delay_Ht_4_mux 						: STD_LOGIC_VECTOR(4 DOWNTO 0):=(OTHERS => '0');
signal signal_enable_FULLY_1 				: STD_LOGIC := '0';
signal extra_output_MINI_MIND_2 			: STD_LOGIC_VECTOR (15 DOWNTO 0);
signal extra_HT_MEMORY_FULLY_1_1 		: STD_LOGIC_VECTOR (15 DOWNTO 0);
signal result_RELU_FULLY_2_2      		: STD_LOGIC_VECTOR (15 DOWNTO 0):=(OTHERS => '0');	
signal signal_enable_FULLY_2				: STD_LOGIC:= '0';
signal INPUT_BUFFER_1_input     			: STD_LOGIC_VECTOR (15 DOWNTO 0);
signal counter_signaltap_add				: STD_LOGIC_VECTOR (5  DOWNTO 0);
signal extra_enable_FULLY_2_1				: STD_LOGIC ;

begin


------INPUT BFFER
INPUT_BUFFER_1 : INPUT_BUFFER port map(
						clk_INPUT_BUFFER 			=>clk_MINI_MIND,
						reset_INPUT_BUFFER 		=>reset_MINI_MIND,
						input_nios_bus_data		=>input_nios_bus_data,
						address_nios_ii			=>address_nios_ii,
						address_write				=>address_write,
						enable_MINI_MIND 			=>enable_MINI_MIND,
						result_INPUT_BUFFER     =>INPUT_BUFFER_1_input);
											

LSTM_1_UNIT : LSTM_1 port map(
						clk_LSTM_1 	 				=>clk_MINI_MIND,
						reset_LSTM_1 				=>reset_MINI_MIND,
						enable_LSTM_1 				=>enable_MINI_MIND,
						data_INPUT_LSTM_Wx		=>INPUT_BUFFER_1_input,
						--nios_ii 
						enable_write_memory		=>enable_write_memory,
						write_memory_select		=>write_memory_select(11 downto 0),		
						dataa_memory_W  			=>dataa_memory_W,
						address_memory_W  		=>address_memory_W,
						-------
						output_LSTM_1	      	=>extra_output_MINI_MIND_1);
	
LSTM_2_UNIT : LSTM_2 port map(
						clk_LSTM_2					=>clk_MINI_MIND,
						reset_LSTM_2 				=>reset_MINI_MIND,
						enable_LSTM_1				=>signal_enable_LSTM_2,
						data_INPUT_LSTM_Wx		=>extra_output_MINI_MIND_1 ,
						--nios_ii 
						enable_write_memory		=>enable_write_memory,
						write_memory_select		=>write_memory_select(23 downto 12),		
						dataa_memory_W  			=>dataa_memory_W,
						address_memory_W  		=>address_memory_W,
						-------
						output_LSTM_1	      	=>extra_output_MINI_MIND_2);
					

------FULLY_1
UNIT_FULLY_1 : FULLY_1 	port map(
						clk_FULLY_1 	 				=>clk_MINI_MIND,
						reset_FULLY_1					=>reset_MINI_MIND,
						enable_FULLY_1 				=>signal_enable_FULLY_1,
						data_FULLY_1_Wh 				=>extra_output_MINI_MIND_2,
						--nios_ii 
						enable_write_memory			=>enable_write_memory,
						write_memory_select			=>write_memory_select(25 downto 24),
						dataa_memory_W  				=>dataa_memory_W,
						address_memory_W  			=>address_memory_W,
						-------
						HT_MEMORY_FULLY_1     		=>extra_HT_MEMORY_FULLY_1_1);



UNIT_FULLY_2:  FULLY_2 port map(																		
						clk_FULLY_2 	 				=>clk_MINI_MIND,
						reset_FULLY_2  				=>reset_MINI_MIND,
						enable_FULLY_2 				=>signal_enable_FULLY_2,
						data_FULLY_2_Wh 				=>extra_HT_MEMORY_FULLY_1_1,
						result_RELU_FULLY_2      	=>result_RELU_FULLY_2_2,
						extra_output_fully_2		   =>extra_enable_FULLY_2_1,
						--nios_ii 
						enable_write_memory			=>enable_write_memory,
						write_memory_select			=>write_memory_select(27 downto 26),
						dataa_memory_W  				=>dataa_memory_W,
						address_memory_W  			=>address_memory_W);


------OUTPUT FOR NIOSII
output_niosii_1 : OUTPUT_NIOSII port map (
						clk_OUTPUT_NIOSII 	 		=>clk_MINI_MIND,
						reset_OUTPUT_NIOSII			=>reset_MINI_MIND,
						enable_OUTPUT_NIOSII 		=>enable_MINI_MIND,
						enable_WRITE_OUTPUT_NIOSII =>extra_enable_FULLY_2_1,
						data_INPUT_OUTPUT_NIOSII 	=>result_RELU_FULLY_2_2,
						counter_signaltap 			=>counter_signaltap_add,
						address_NIOSII					=>address_NIOSII,
						result_OUTPUT_NIOSII 		=>result_OUTPUT_NIOSII);
						

-----enable_LSTM_2
process (clk_MINI_MIND,enable_MINI_MIND)
begin  
if(clk_MINI_MIND 'event and clk_MINI_MIND ='1') then

		if (reset_MINI_MIND = '1') then
							delay_Ht_1_mux <=(OTHERS => '0');					---
							counter_1mux <=(OTHERS => '0');						---
							extra_1mux  <= (OTHERS => '0');						---	
							counter_address_1mux <= (OTHERS => '0');			---
							signal_enable_LSTM_2 <= '0';							---
		end if;
		if (enable_MINI_MIND = '1') then
							if ( counter_1mux > "01111111") then  --128
											if ( extra_1mux= "100") then  --DELAY CLEAR MAC
													counter_1mux <=(OTHERS => '0');
													extra_1mux  <= (OTHERS => '0');
													counter_address_1mux <= counter_address_1mux+1;
											else		
													extra_1mux <= extra_1mux + 1;
											end if;
							else	
											counter_1mux <= counter_1mux +1;
											
							end if;
			
				
							if ( counter_address_1mux > "01111111") then
											counter_1mux <=(OTHERS => '0');
											extra_1mux  <= (OTHERS => '0');
											
											if (delay_Ht_1_mux > "11000") then ---11000
													counter_address_1mux <= (OTHERS => '0');
													delay_Ht_1_mux <= (OTHERS => '0');
													signal_enable_LSTM_2 <= '1';
													
											else 
													delay_Ht_1_mux <= delay_Ht_1_mux +1;

											end if;
						  end if;
							
		end if;
	
			
end if;
				
end process;


-----enable_FULLY_1
process (clk_MINI_MIND,signal_enable_LSTM_2)
begin  
if(clk_MINI_MIND 'event and clk_MINI_MIND ='1') then

		if (reset_MINI_MIND = '1') then
							counter_2mux <=(OTHERS => '0');						---
							extra_2mux  <= (OTHERS => '0');						---
							signal_enable_FULLY_1<= '0';							---
							counter_address_2mux <= (OTHERS => '0');			---
							delay_Ht_2_mux <= (OTHERS => '0');					---
		end if;
	
		if (signal_enable_LSTM_2= '1') then
							if ( counter_2mux > "01111111") then  --128
											if ( extra_2mux= "100") then  --DELAY CLEAR MAC
													counter_2mux <=(OTHERS => '0');
													extra_2mux  <= (OTHERS => '0');
													counter_address_2mux <= counter_address_2mux+1;
											else		
													extra_2mux <= extra_2mux + 1;
											end if;
							else	
											counter_2mux <= counter_2mux +1;
											
							end if;
			
				
							if ( counter_address_2mux > "01111111") then
											counter_2mux <=(OTHERS => '0');
											extra_2mux  <= (OTHERS => '0');
											
											if (delay_Ht_2_mux > "11000") then ---11000
													counter_address_2mux <= (OTHERS => '0');
													delay_Ht_2_mux <= (OTHERS => '0');
													signal_enable_FULLY_1<= '1';
													
											else 
													delay_Ht_2_mux <= delay_Ht_2_mux +1;

											end if;
						  end if;
							
		end if;
	
			
end if;
				
end process;

-----enable_FULLY_2
process (clk_MINI_MIND,signal_enable_FULLY_1)
begin  
if(clk_MINI_MIND 'event and clk_MINI_MIND ='1') then

		if (reset_MINI_MIND = '1') then
							counter_3mux <=(OTHERS => '0');					---
							extra_3mux  <= (OTHERS => '0');					---
							counter_address_3mux <= (OTHERS => '0');		---
							delay_Ht_3_mux <= (OTHERS => '0');				---
							signal_enable_FULLY_2 <= '0';						---
		end if;
	
		if (signal_enable_FULLY_1= '1') then
							if ( counter_3mux > "01111111") then  --128
											if ( extra_3mux= "100") then  --DELAY CLEAR MAC
													counter_3mux <=(OTHERS => '0');
													extra_3mux  <= (OTHERS => '0');
													counter_address_3mux <= counter_address_3mux+1;
											else		
													extra_3mux <= extra_3mux + 1;
											end if;
							else	
											counter_3mux <= counter_3mux +1;
											
							end if;
			
				
							if ( counter_address_3mux > "01111111") then
											counter_3mux <=(OTHERS => '0');
											extra_3mux  <= (OTHERS => '0');
											
											if (delay_Ht_3_mux > "11000") then ---11000
													counter_address_3mux <= (OTHERS => '0');
													delay_Ht_3_mux <= (OTHERS => '0');
													signal_enable_FULLY_2<= '1';
													
											else 
													delay_Ht_3_mux <= delay_Ht_3_mux +1;

											end if;
						  end if;
							
		end if;
	
			
end if;
				
end process;



-----counter_SIGNALTAP
process (clk_MINI_MIND,enable_MINI_MIND)
begin  
if(clk_MINI_MIND 'event and clk_MINI_MIND ='1') then

		if (reset_MINI_MIND = '1') then
							counter_4mux <=(OTHERS => '0');							---
							extra_4mux  <= (OTHERS => '0');							---
							counter_address_4mux <= (OTHERS => '0');				---
							delay_Ht_4_mux <= (OTHERS => '0');						---
							counter_signaltap_add <= (OTHERS => '0');
		
		end if;
		if (enable_MINI_MIND = '1') then
							if ( counter_4mux > "01111111") then  --128
											if ( extra_4mux= "100") then  --DELAY CLEAR MAC
													counter_4mux <=(OTHERS => '0');
													extra_4mux  <= (OTHERS => '0');
													counter_address_4mux <= counter_address_4mux+1;
											else		
													extra_4mux <= extra_4mux + 1;
											end if;
							else	
											counter_4mux <= counter_4mux +1;
											
							end if;
			
				
							if ( counter_address_4mux > "01111111") then
											counter_4mux <=(OTHERS => '0');
											extra_4mux  <= (OTHERS => '0');
											
											if (delay_Ht_4_mux > "11000") then ---11000
													counter_address_4mux <= (OTHERS => '0');
													delay_Ht_4_mux <= (OTHERS => '0');
													if (counter_signaltap_add > "100111") then ---11000
															counter_signaltap_add <= counter_signaltap_add;
													else 
															counter_signaltap_add <= counter_signaltap_add +1;
													end if;
													
													
											else 
													delay_Ht_4_mux <= delay_Ht_4_mux +1;

											end if;
						  end if;
							
		end if;
	
			
end if;
				
end process;



end Behavioral;