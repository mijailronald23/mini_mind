library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.std_logic_unsigned.all;



entity reg_32_read is 
	port( clock,resetn : in std_logic;
			--D				 : in std_logic_vector(31 downto 0);
			local_byteenable_read : in std_logic_vector(3 downto 0);
			Q				 : out std_logic_vector(31 downto 0));
			
end reg_32_read;

architecture behavior of reg_32_read is 

signal D_1 	 		 : std_logic_vector(7 downto 0):="00000000";
signal delay_1 	 : std_logic:='0';
begin 
	process
	begin 
			wait until clock'event and clock ='1';
			if local_byteenable_read (0)= '1' then		
					if delay_1= '1' then		
							D_1 <= D_1 + '1';
							delay_1 <= '0';
							--Q (7 downto 0) <= D_1;
					else 
							delay_1 <='1';
					end if;
					if D_1 = "00000000" then
								Q <= "00000000000000000000000010000000";		
					end if;
					
					if D_1 = "00000001" then
								Q <= "00000000000000000000000010001000";		
					end if;					
					
					if D_1 = "00000010" then
								Q <= "00000000000000000000000000001000";		
					end if;
					if D_1 = "00000011" then
								Q <= "00000000000000001000100000001000";		
					end if;
					if D_1 = "00000100" then
								Q <= "00000000000000000000100000001000";		
					end if;					
					
			end if;
	end process;
end behavior;

