library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.std_logic_unsigned.all;

entity reg32_avalon_interface is
	port ( clock,resetn 			  : in std_logic;
			 read_1,write_1,chipselect : in std_logic;
			 writedata				  : in std_logic_vector(31 downto 0);
			 byteenable   			  : in std_logic_vector(3 downto 0);
			 readdata	  			  : out std_logic_vector(31 downto 0);
			 Q_export 				  : out std_logic_vector(31 downto 0));
end reg32_avalon_interface;

architecture structure of reg32_avalon_interface is 
	signal local_byteenable : std_logic_vector(3 downto 0);
	signal local_byteenable_read : std_logic_vector(3 downto 0);
	signal to_reg,from_reg  : std_logic_vector(31 downto 0);
	signal extra_1  : std_logic_vector(31 downto 0);
	signal extra_2  : std_logic;
	component reg32 
	port( clock,resetn : in std_logic;
			D				 : in std_logic_vector(31 downto 0);
			byteenable   : in std_logic_vector(3 downto 0);
			Q				 : out std_logic_vector(31 downto 0));
			
	end component;
	
	component reg_32_read 
	port( clock,resetn : in std_logic;
			--D				 : in std_logic_vector(31 downto 0);
			local_byteenable_read : in std_logic_vector(3 downto 0);
			Q				 : out std_logic_vector(31 downto 0));
			
	end component;

	
begin 
	to_reg <= writedata;
	with (chipselect and write_1) select	
			local_byteenable <= byteenable when '1', "0000" when others;
	with (chipselect and read_1) select	
			local_byteenable_read <= byteenable when '1', "0000" when others;			
			
	reg_instance: reg32 port map(clock,resetn,to_reg,local_byteenable,from_reg);
	reg_instance_1:reg_32_read  port map(clock,resetn,local_byteenable_read,extra_1);
	
	--extra_2 <= chipselect and read_1;
	--end if;
	readdata <= extra_1;
	Q_export <= extra_1;


end structure;