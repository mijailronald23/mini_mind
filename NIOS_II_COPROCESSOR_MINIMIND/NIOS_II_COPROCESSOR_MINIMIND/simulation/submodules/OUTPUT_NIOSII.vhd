library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

entity OUTPUT_NIOSII is               
  port(
		clk_OUTPUT_NIOSII 	 						: IN STD_LOGIC  ;
		reset_OUTPUT_NIOSII	 		 	 			: IN STD_LOGIC  ;
		enable_OUTPUT_NIOSII 						: IN STD_LOGIC  ;
		enable_WRITE_OUTPUT_NIOSII 				: IN STD_LOGIC  ;
		data_INPUT_OUTPUT_NIOSII 	 				: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		counter_signaltap 							: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		address_NIOSII									: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		result_OUTPUT_NIOSII 				     	: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end OUTPUT_NIOSII;


architecture Behavioral of OUTPUT_NIOSII is 

component UNIT_OUTPUT_1 port(
		aclr		: IN STD_LOGIC  := '0';
		address		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		clock			: IN STD_LOGIC  ;
		data			: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		rden			: IN STD_LOGIC  ;
		wren			: IN STD_LOGIC ;
		q				: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;

signal delay_Ht_1 		 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');


signal address_H_2					: STD_LOGIC_VECTOR (6 DOWNTO 0)	 :="1111111";
signal read_enable_2					: STD_LOGIC;
signal write_enable_2				: STD_LOGIC;
signal output_signal_niosii_1		: STD_LOGIC;
signal memory_output_2_cell		: STD_LOGIC_VECTOR (15 DOWNTO 0)	 :=(OTHERS => '0');

begin

							
UNIT_MEMORY_1_OUTPUT_NIOSII : UNIT_OUTPUT_1 port map(
							aclr			=>reset_OUTPUT_NIOSII,
							address		=>address_H_2,
							clock			=>clk_OUTPUT_NIOSII,
							data			=>data_INPUT_OUTPUT_NIOSII,
							rden			=>read_enable_2,
							wren			=>write_enable_2,
							q				=>memory_output_2_cell);
							
result_OUTPUT_NIOSII  <= memory_output_2_cell;

---MEMORY-CONFIGURATION-FOR READ AND WRITE				
process (clk_OUTPUT_NIOSII,enable_WRITE_OUTPUT_NIOSII)
begin  
if(clk_OUTPUT_NIOSII 'event and clk_OUTPUT_NIOSII ='1') then


	if (reset_OUTPUT_NIOSII = '1') then
			read_enable_2  <= '0';
			write_enable_2 <= '0';
			address_H_2 <="1111111";
	end if;
	if (enable_OUTPUT_NIOSII = '1') then
				if (counter_signaltap = "100010") then
							read_enable_2  <= '0';
							if (enable_WRITE_OUTPUT_NIOSII = '1') then	
											write_enable_2 <= '1';			
											address_H_2 <= address_H_2 +1;
									
							end if;
							if (enable_WRITE_OUTPUT_NIOSII = '0') then	
											write_enable_2 <= '0';											--NO WRITE MEMORY_2	
									
							end if;			
				end if;
				if (counter_signaltap > "100010") then
								read_enable_2  <= '1';
								write_enable_2 <= '0';
								address_H_2 <= address_NIOSII;
				
				end if ;					
	end if ;		
					



end if;
end process;


							
end Behavioral;
