library IEEE;
use IEEE.std_logic_1164.all;

entity logic_comparator_tanh is               
  port(
	   
		clk_comparator	      : IN STD_LOGIC  ;
		--enable_comparator		: IN STD_LOGIC  ;
		dataa_input				: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result_1    			: OUT STD_LOGIC_VECTOR (3 DOWNTO 0));
end logic_comparator_tanh;

architecture Behavioral of logic_comparator_tanh is 
signal signal_comparator : STD_LOGIC :='0';

signal vector_1 : STD_LOGIC:=  '0' ;
signal vector_2 : STD_LOGIC:=  '0' ;
signal vector_comparator : STD_LOGIC_VECTOR (10 DOWNTO 0):=(OTHERS => '0') ;
signal vector_comparator2 : STD_LOGIC_VECTOR (11 DOWNTO 0):=(OTHERS => '0') ;
signal output_1 : STD_LOGiC_VECTOR (3 DOWNTO 0):=(OTHERS => '0') ;

begin
signal_comparator <= dataa_input(15);
vector_1 <= dataa_input(14) or dataa_input(13) or dataa_input(12) or dataa_input(11); --positive case search 1' values 
vector_2 <= dataa_input(14) and dataa_input(13) and dataa_input(12) and dataa_input(11); --negative case search 0' values 

vector_comparator <= dataa_input (10 DOWNTO 0);
--vector_comparator2 <= dataa_input (11 DOWNTO 0);

result_1 <= output_1;
process (clk_comparator)
begin  
if(clk_comparator'event and clk_comparator='1') then
		
		if (signal_comparator = '1') then  --case negatives
				if (vector_2 ='0') then
						output_1 <= "0001"; --1 region -8
				else
						if (vector_comparator >="11110000000") then
								output_1 <= "0111"; --7 region -0.5
						else										
								if (vector_comparator >="11010000000") then
										output_1 <= "0110"; --6 region -1.5 complement two
								else		
										if (vector_comparator >="10110000000") then
												output_1 <= "0101"; --5 region -2.5 complement two
										else
												if (vector_comparator >="10010000000") then
														output_1 <= "0100"; --4 region -3.5 complement two
												else
														if (vector_comparator >= "01110000000") then
																output_1 <= "0011"; --3 region -4.5 complement two
																
														else
																output_1 <= "0010"; --2 region -5.5 complement two
																
														end if;
												end if;
										end if;
								end if;
						end if;
				end if;		
		end if;	-- case positive 
		if (signal_comparator = '0') then  -- case positive 
			if (vector_1 ='1') then
				output_1 <= "1101"; --13 region +8
			else 
				if (vector_comparator >="10010000000") then
					output_1 <= "1100"; --12 region +4.5
				else 
					if (vector_comparator >="01110000000") then
						output_1 <= "1011"; --11 region +3.5
					else 
						if (vector_comparator >="01010000000") then
							output_1 <= "1010"; --10 region +2.5
						else
							if (vector_comparator >="00110000000") then
								output_1 <= "1001"; --9 region +1.5
							else
								if (vector_comparator >="00010000000") then
								   output_1 <= "1000"; --8 region +0.5
								else
									output_1 <= "0111"; --7 region -0.5
								end if;
							end if;
						end if;	
					end if;
				end if;
			end if ;	
			
		end if;

		
end if;
end process;

end Behavioral;