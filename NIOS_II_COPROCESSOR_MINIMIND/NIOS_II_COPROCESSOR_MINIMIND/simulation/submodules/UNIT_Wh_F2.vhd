LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

entity UNIT_Wh_F2 is
  port(
		clk_UNIT_Wh_F2 			: IN STD_LOGIC  ;
		reset_UNIT_Wh_F2 			: IN STD_LOGIC  ;
		enable_UNIT_Wh_F2 		: IN STD_LOGIC  ;
		dataa_input_Wh				: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		signal_ready_Wh  			: OUT STD_LOGIC  ;
		--nios_ii 
		enable_write_memory		: IN STD_LOGIC  ;
		write_memory_1				: IN STD_LOGIC  ;
		dataa_memory_Wh  			: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_Wh 		: IN STD_LOGIC_VECTOR (13	DOWNTO 0);
		----			
		result_Wh        : OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end UNIT_Wh_F2;

architecture Behavioral of UNIT_Wh_F2 is 

--Mac component 16 bits (Signed)x 16 bits(Signed) 
component UNIT_MAC port(
		aclr3			: IN STD_LOGIC;
		clock0		: IN STD_LOGIC;
		dataa			: IN STD_LOGIC_VECTOR;
		datab			: IN STD_LOGIC_VECTOR;
		ena0			: IN STD_LOGIC ;
		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
end component;


--Matrix of Wh(128 x 128)
component UNIT_RAM_Wh port(
		aclr			: IN STD_LOGIC  := '0';
		address		: IN STD_LOGIC_VECTOR (13 DOWNTO 0);
		clock			: IN STD_LOGIC ;
		data			: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		rden			: IN STD_LOGIC ;
		wren			: IN STD_LOGIC ;
		q				: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
		
	);
end component;


signal address_ram_1 			 	: STD_LOGIC_VECTOR (13 DOWNTO 0):="11111111111111";
signal signal_ram1					: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal clear_1	 					 	: STD_LOGIC  :='0';
signal mac_enable	 		   	 	: STD_LOGIC  :='0';
signal read_memory_enable		 	: STD_LOGIC  :='0';
signal counter_2		 			 	: STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal signal_ready_1 			 	: STD_LOGIC  :='0';
signal counter_address			 	: STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter3		 			 	: STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter4		 			 	: STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal extra		 				 	: STD_LOGIC_VECTOR (2 DOWNTO 0)  :=(OTHERS => '0');
signal extra_mac_1 				 	: STD_LOGIC_VECTOR (31 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_1 				 	: STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_2 		 		 	: STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_3 				 	: STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_1	 		 	: STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_2	 		 	: STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_HT_address_memory : STD_LOGIC_VECTOR (6 DOWNTO 0):="1111111";
signal delay_reset  					: STD_LOGIC_VECTOR (1 DOWNTO 0)  :=(OTHERS => '0');
--nios ii variables 
signal data_input_memory	 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal write_memory_value   : STD_LOGIC  :='0';

begin

MAC_Wh : UNIT_MAC port map(
						aclr3		=>clear_1,
						clock0	=>clk_UNIT_Wh_F2 ,
						dataa		=>dataa_input_Wh,
						datab		=>signal_ram1,
						ena0		=>mac_enable,
						result	=>extra_mac_1 );
						
result_Wh (14 DOWNTO 0)<= extra_mac_1(22 DOWNTO 8); --Resize memory 
result_Wh (15)<= extra_mac_1(31);						
						

RAM_Wh : UNIT_RAM_Wh port map(
						aclr		=>reset_UNIT_Wh_F2,
						address	=>address_ram_1,
						clock		=>clk_UNIT_Wh_F2 ,
						data		=>data_input_memory,
						rden		=>read_memory_enable,
						wren		=>write_memory_value,
						q			=>signal_ram1);


signal_ready_Wh <= signal_ready_1;

				
process (clk_UNIT_Wh_F2,enable_UNIT_Wh_F2)
begin  
	if(clk_UNIT_Wh_F2'event and clk_UNIT_Wh_F2='1') then
	
			if (enable_write_memory = '1') then 				--write memory unit
					address_ram_1				<= address_memory_Wh;
					data_input_memory			<= dataa_memory_Wh;
					write_memory_value		<= write_memory_1;
			end if;

	
			if (reset_UNIT_Wh_F2 = '1') then
					counter_2 <=(OTHERS => '0');
					extra  <= (OTHERS => '0');
					read_memory_enable <= '0';
					mac_enable	<= '0';
					address_ram_1     <= "11111111111111";
					counter_HT_address_memory <= "1111111";
					counter_address <= (OTHERS => '0');
					delay_Ht_1 <= (OTHERS => '0');		
			
			end if;
			if (enable_UNIT_Wh_F2 = '1') then 
					read_memory_enable <= '1';
					mac_enable	<= '1';
					if ( counter_2 > "01111111") then  --128
									if ( extra= "100") then  --DELAY CLEAR MAC
										counter_2 <=(OTHERS => '0');
										extra  <= (OTHERS => '0');
										counter_address <= counter_address +1;
									else	
										extra <= extra + 1;
									end if;
					else	
									counter_2 <= counter_2 +1;
									address_ram_1 <= address_ram_1 +1;
									counter_HT_address_memory <= counter_HT_address_memory+1;
					end if;
			end if;
			
			if (enable_UNIT_Wh_F2 = '0') then  
									read_memory_enable <= '0';
									mac_enable	<= '0';
			end if;
				
			if ( counter_address > "01111111") then
									counter_2 <=(OTHERS => '0');
									extra  <= (OTHERS => '0');
									read_memory_enable <= '0';
									mac_enable	<= '0';
									address_ram_1     <= "11111111111111";
									counter_HT_address_memory <= "1111111";
									if (delay_Ht_1 > "11000") then
											counter_address <= (OTHERS => '0');
											delay_Ht_1 <= (OTHERS => '0');
							
									else 
											delay_Ht_1 <= delay_Ht_1 +1;

									end if;
			end if;
				

	end if;
end process;



-----CLEAR MAC
process (clk_UNIT_Wh_F2,enable_UNIT_Wh_F2)
begin  
	if(clk_UNIT_Wh_F2'event and clk_UNIT_Wh_F2='1') then
	
				if (reset_UNIT_Wh_F2 = '1') then 
							counter_128_Ht_1     <= (OTHERS => '0');
							delay_Ht_2 <= (OTHERS => '0');
							counter3  <= (OTHERS => '0');
							if (delay_reset = "11") then	

									clear_1 <= '0';
							else	
									clear_1 <= '1';
									delay_reset <= delay_reset +'1';
							end if;	
				end if;
				if (enable_UNIT_Wh_F2 = '1') then 
							delay_reset <= (OTHERS => '0');
		
							if ( counter3= "10000110") then
									clear_1 <= '1';
									counter3 <="00000010";
									counter_128_Ht_1 <= counter_128_Ht_1 +1;
							else	
									counter3 <= counter3 +1;
									clear_1 <= '0';	
							end if;
	
				end if;
				
				if (enable_UNIT_Wh_F2 = '0') then 
							counter3 <= (OTHERS => '0');
				end if;
				
				if ( counter_128_Ht_1 > "01111111") then
							clear_1 <= '1';
							counter3 <="00000010";
							if (delay_Ht_2 > "11000") then  				
									counter_128_Ht_1     <= (OTHERS => '0');
									delay_Ht_2 <= (OTHERS => '0');
							
							else 
									delay_Ht_2 <= delay_Ht_2 +1;
							
							end if;
					
				end if;

	end if;
end process;

-----SIGNAL-READY
process (clk_UNIT_Wh_F2,enable_UNIT_Wh_F2)
begin  
	if(clk_UNIT_Wh_F2'event and clk_UNIT_Wh_F2='1') then
	
	
				if (reset_UNIT_Wh_F2 = '1') then 
							counter_128_Ht_2     <= (OTHERS => '0');
							delay_Ht_3 <= (OTHERS => '0');
							counter4	  <= (OTHERS => '0');
							signal_ready_1	<= '0';
				end if;
				if (enable_UNIT_Wh_F2 = '1') then 
		
							if ( counter4= "10000101") then
									signal_ready_1	<= '1';
									counter4 <="00000001";
									counter_128_Ht_2 <= counter_128_Ht_2 +1;
							else	
									counter4 <= counter4 +1;
									signal_ready_1	<= '0';
									
							end if;
	
				end if;
				
				if (enable_UNIT_Wh_F2 = '0') then 
							counter4 <= (OTHERS => '0');
				end if;
				
				if ( counter_128_Ht_2 > "01111111") then
					counter4 <="00000001";
					if (delay_Ht_3 > "11000") then
							counter_128_Ht_2     <= (OTHERS => '0');
							delay_Ht_3 <= (OTHERS => '0');
							
					else 
							delay_Ht_3 <= delay_Ht_3 +1;
							
					end if;
				end if;
				
				
				


	end if;
end process;



end Behavioral;