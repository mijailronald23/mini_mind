library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

entity BASIC_UNIT_1 is port(
		clk_BASIC_UNIT_1 	 				: IN STD_LOGIC  ;
		reset_BASIC_UNIT_1 	 			: IN STD_LOGIC  ;
		enable_BASIC_UNIT_1 				: IN STD_LOGIC  ;
		data_BASIC_UNIT_1_Wh 			: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		data_BASIC_UNIT_1_Wx 			: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		enable_OUTPUT_BASIC_UNIT_1		: OUT STD_LOGIC  ;
		--nios_ii 
		enable_write_memory				: IN STD_LOGIC  ;
		write_memory_select				: IN STD_LOGIC_VECTOR (2	DOWNTO 0);
		dataa_memory_W  					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_W  				: IN STD_LOGIC_VECTOR (13 DOWNTO 0);
		----
		result_BASIC_UNIT_1      		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end BASIC_UNIT_1;


architecture Behavioral of BASIC_UNIT_1 is 


----UNIT_Wh
component UNIT_1_Wh
  port(
		clk_UNIT_1_Wh 			: IN STD_LOGIC  ;
		reset_Wh 				: IN STD_LOGIC  ;
		enable_UNIT_1_Wh 		: IN STD_LOGIC  ;
		dataa_input_Wh			: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		signal_ready_Wh  		: OUT STD_LOGIC  ;
		--nios_ii 
		enable_write_memory	: IN STD_LOGIC  ;
		write_memory_1			: IN STD_LOGIC  ;
		dataa_memory_Wh  		: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_Wh 	: IN STD_LOGIC_VECTOR (13	DOWNTO 0);
		----	
		result_Wh        		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;

----UNIT_Wx
component UNIT_1_Wx      
  port(
		clk_UNIT_1_Wx 			: IN STD_LOGIC  ;
		reset_Wx					: IN STD_LOGIC  ;
		enable_UNIT_1_Wx 		: IN STD_LOGIC  ;
		dataa_input_Wx   		: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		signal_ready_Wx 		: OUT STD_LOGIC  ;
		--nios_ii 
		enable_write_memory	: IN STD_LOGIC  ;
		write_memory_1			: IN STD_LOGIC  ;
		dataa_memory_Wx  		: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_Wx 	: IN STD_LOGIC_VECTOR (8 DOWNTO 0);
		----
		result_Wx     		   : OUT STD_LOGIC_VECTOR (15 DOWNTO 0));  
end component;

----UNIT_B
component UNIT_RAM_B 
	port(
		aclr		: IN STD_LOGIC  := '0';
		address	: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		data		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		rden		: IN STD_LOGIC  := '1';
		wren		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
end component;

----UNIT_ADDER
component UNIT_GATE_1_ADDER port(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result	: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
end component;


----UNIT_REGISTER
component UNIT_REGISTER_1 port(
		clock		: IN STD_LOGIC ;
		data		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		enable	: IN STD_LOGIC ;
		load		: IN STD_LOGIC ;
		sclr		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
end component;


signal result_adder_1		 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_adder_2		 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_memory_B		  			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_Wh		 		 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_Wx		 		 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_Wh_1		 	 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_Wx_1		 	 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal address_ram_1   		 			 : STD_LOGIC_VECTOR (6 DOWNTO 0)	  :="1111111"; --INIT VALUE OF ADDRESS 
signal counter_B     		 			 : STD_LOGIC_VECTOR (7 DOWNTO 0)	  :=(OTHERS => '0');
signal counter_register_output 		 : STD_LOGIC_VECTOR (7 DOWNTO 0)	  :=(OTHERS => '0');
signal counter_register_2 	 			 : STD_LOGIC_VECTOR (7 DOWNTO 0)	  :=(OTHERS => '0');
signal signal_ready_Wh_1 	 			 : STD_LOGIC 							  :='0';
signal signal_ready_Wx_1 	 			 : STD_LOGIC 							  :='0';
signal enable_UNIT_1_Wh_1  			 : STD_LOGIC 							  :='0';
signal enable_UNIT_1_Wx_1  			 : STD_LOGIC 							  :='0';
signal init_bias            			 : STD_LOGIC 							  :='0';
signal enable_register		 			 : STD_LOGIC 							  :='0';
signal read_memory_B       			 : STD_LOGIC 							  :='0';
signal enable_register_1	 			 : STD_LOGIC 							  :='0';
signal delay_Ht_1 		 				 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_2 		 				 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_3 		 				 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_1	 				 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_2	 				 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_3	 				 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');

--nios ii variables 
signal data_input_memory	 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal write_memory_value   			 : STD_LOGIC  :='0';
begin


UNIT_GATE_1_Wh : UNIT_1_Wh port map(
							clk_UNIT_1_Wh 				=>clk_BASIC_UNIT_1,
							reset_Wh						=>reset_BASIC_UNIT_1,
							enable_UNIT_1_Wh 			=>enable_UNIT_1_Wh_1,
							dataa_input_Wh				=>data_BASIC_UNIT_1_Wh,
							signal_ready_Wh 			=>signal_ready_Wh_1,
							--nios_ii 
							enable_write_memory		=>enable_write_memory,
							write_memory_1				=>write_memory_select(0),
							dataa_memory_Wh  			=>dataa_memory_W,
							address_memory_Wh 		=>address_memory_W,
							---
							result_Wh        			=>result_Wh);
							
							
UNIT_GATE_1_Wx : UNIT_1_Wx port map(
							clk_UNIT_1_Wx 				=>clk_BASIC_UNIT_1,
							reset_Wx						=>reset_BASIC_UNIT_1,
							enable_UNIT_1_Wx 			=>enable_UNIT_1_Wx_1,
							dataa_input_Wx				=>data_BASIC_UNIT_1_Wx,
							signal_ready_Wx 			=>signal_ready_Wx_1,	
							--nios_ii 
							enable_write_memory		=>enable_write_memory,
							write_memory_1				=>write_memory_select(1),
							dataa_memory_Wx  			=>dataa_memory_W,
							address_memory_Wx 		=>address_memory_W(8 DOWNTO 0),
							---						
							result_Wx        			=>result_Wx);
							

							
UNIT_GATE_1_B 	: UNIT_RAM_B port map(
							aclr							=>reset_BASIC_UNIT_1,
							address						=>address_ram_1,
							clock							=>clk_BASIC_UNIT_1,
							data							=>data_input_memory,
							rden							=>read_memory_B,
							wren							=>write_memory_value,
							q								=>result_memory_B);
							

							
UNIT_GATE_ADDER_1 : UNIT_GATE_1_ADDER port map(
								clock						=>clk_BASIC_UNIT_1,
								dataa						=>result_Wh_1,
								datab						=>result_Wx_1,
								result					=>result_adder_1);

								

UNIT_GATE_ADDER_2 : UNIT_GATE_1_ADDER port map(
								clock						=>clk_BASIC_UNIT_1,
								dataa						=>result_adder_1,
								datab						=>result_memory_B,
								result					=>result_adder_2);
								
UNIT_REGISTER : 	UNIT_REGISTER_1 port map(
								clock						=>clk_BASIC_UNIT_1,
								data						=>result_adder_2,
								enable					=>enable_register,
								load						=>enable_register,
								sclr						=>reset_BASIC_UNIT_1,
								q							=>result_BASIC_UNIT_1);
							
enable_OUTPUT_BASIC_UNIT_1 <= enable_register_1;


--Syncronize Wh and Wx
enable_Wx_Wh :process (clk_BASIC_UNIT_1,enable_BASIC_UNIT_1)
begin  
	if(clk_BASIC_UNIT_1'event and clk_BASIC_UNIT_1='1') then
				if (enable_BASIC_UNIT_1='1') then 
						enable_UNIT_1_Wh_1 <= '1';
						enable_UNIT_1_Wx_1 <= '1';
				end if;
				if (enable_BASIC_UNIT_1='0') then
						enable_UNIT_1_Wh_1 <= '0';
						enable_UNIT_1_Wx_1 <= '0';
				end if;

	end if;
end process enable_Wx_Wh;


						
--ADDER-1-Wh
process_adder_Wh :process (clk_BASIC_UNIT_1,signal_ready_Wh_1)
begin  
	if(clk_BASIC_UNIT_1'event and clk_BASIC_UNIT_1='1') then
				if (signal_ready_Wh_1 = '1') then 
						result_Wh_1 <= result_Wh;
				end if;
				if (signal_ready_Wh_1 = '0') then 
						result_Wh_1 <= (OTHERS => '0');
				end if;

	end if;
end process process_adder_Wh;


--ADDER-1-Wx
process_adder_Wx :process (clk_BASIC_UNIT_1,signal_ready_Wx_1)
begin  
	if(clk_BASIC_UNIT_1'event and clk_BASIC_UNIT_1='1') then
				if (signal_ready_Wx_1 = '1') then 
						result_Wx_1 <= result_Wx;
				end if;
				if (signal_ready_Wx_1 = '0') then 
						result_Wx_1 <= (OTHERS => '0');
				end if;

	end if;
end process process_adder_Wx;


--Read B - values 
unit_memory_bias: process (clk_BASIC_UNIT_1,enable_UNIT_1_Wh_1,enable_UNIT_1_Wx_1)
begin  
	if(clk_BASIC_UNIT_1'event and clk_BASIC_UNIT_1='1') then	
				init_bias <= enable_UNIT_1_Wh_1 and enable_UNIT_1_Wx_1;
				
				if (enable_write_memory = '1') then 				--write memory unit
						address_ram_1				<= address_memory_W(6 DOWNTO 0);
						data_input_memory			<= dataa_memory_W;
						write_memory_value		<= write_memory_select(2);
				end if;				
				
				if (reset_BASIC_UNIT_1 = '1') then
						counter_B <= (OTHERS => '0');						---
						address_ram_1 <= "1111111";						---
						read_memory_B <= '0';								---
						counter_128_Ht_1 <= (OTHERS => '0');			---
						delay_Ht_1 <= (OTHERS => '0');
						
				end if;
				if (init_bias = '1') then
						if (counter_B = "10000011") then    --128(Neurons)+ 3 cycles of delay
					
									read_memory_B <= '1';
									address_ram_1   <= address_ram_1 +1;
									counter_B <= "11111111";
									counter_128_Ht_1 <= counter_128_Ht_1 +1;
						else 
									read_memory_B <= '0';
									counter_B <= counter_B +1;						
						end if;
				
				end if;

				if ( counter_128_Ht_1 > "01111111") then
						counter_B <= "00000001";
						address_ram_1 <= "1111111";
						if (delay_Ht_1 > "11000") then  				
									counter_128_Ht_1 <= (OTHERS => '0');
									delay_Ht_1 <= (OTHERS => '0');
							
						else 
									delay_Ht_1 <= delay_Ht_1 +1;
							
						end if;
					
				end if;
				
				

	end if;
end process unit_memory_bias;


--enable register 
enable_register_process: process (clk_BASIC_UNIT_1,enable_BASIC_UNIT_1)
begin  
if(clk_BASIC_UNIT_1'event and clk_BASIC_UNIT_1='1') then	

				if (reset_BASIC_UNIT_1 = '1') then
							counter_128_Ht_2 <= (OTHERS => '0');
							delay_Ht_2 <= (OTHERS => '0');								---
							enable_register <= '0';											---
							counter_register_output <= (OTHERS => '0');				---			
				end if;
				if (enable_BASIC_UNIT_1= '1') then
							if (counter_register_output > "10001000") then  --128(Neurons)+ x cycles of delay(MAC and MEMORY)
									counter_register_output <="00000101";
									enable_register <= '1';
									counter_128_Ht_2 <= counter_128_Ht_2 +1;
								
							else
									enable_register <= '0';
									counter_register_output <= counter_register_output +1;
						
							end if;
				
				end if;
				

				if ( counter_128_Ht_2 > "01111111") then
						counter_register_output <="00000101";
		
						if (delay_Ht_2 > "11000") then  				
									counter_128_Ht_2 <= (OTHERS => '0');
									delay_Ht_2 <= (OTHERS => '0');
							
						else 
									delay_Ht_2 <= delay_Ht_2 +1;
							
						end if;
					
				end if;				
				
				
end if;
end process enable_register_process;


--enable sigmoid/tanh
enable_process_nonlinear : process (clk_BASIC_UNIT_1,enable_BASIC_UNIT_1)
begin  
	if(clk_BASIC_UNIT_1'event and clk_BASIC_UNIT_1='1') then	
	
				if (reset_BASIC_UNIT_1 = '1') then
				
						counter_128_Ht_3 <= (OTHERS => '0');						---
						delay_Ht_3 <= (OTHERS => '0');								---
						counter_register_2 <= (OTHERS => '0');						---
						enable_register_1 <= '0';										---			
				
				end if;
				if (enable_BASIC_UNIT_1= '1') then
						if  (counter_register_2=  "10001100") then --128(Neurons)+ x cycles of delay(MAC and MEMORY and ADDERS 1-2) 
									enable_register_1 <= '1';
									counter_register_2 <="00001000";  --6 INIT value to continue with the cycle of 128
									counter_128_Ht_3 <= counter_128_Ht_3 +1;
				
								
						else
									enable_register_1 <= '0';
									counter_register_2 <= counter_register_2+1;
						
						end if;
				
				end if;
				
				if ( counter_128_Ht_3 > "01111111") then
						counter_register_2 <="00001000";
						if (delay_Ht_3 > "11000") then  				
									counter_128_Ht_3 <= (OTHERS => '0');
									delay_Ht_3 <= (OTHERS => '0');
							
						else 
									delay_Ht_3 <= delay_Ht_3 +1;
							
						end if;
					
				end if;					
				
				
				
	end if;
end process enable_process_nonlinear;

end Behavioral;