
# (C) 2001-2017 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and 
# other software and tools, and its AMPP partner logic functions, and 
# any output files any of the foregoing (including device programming 
# or simulation files), and any associated documentation or information 
# are expressly subject to the terms and conditions of the Altera 
# Program License Subscription Agreement, Altera MegaCore Function 
# License Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by Altera 
# or its authorized distributors. Please refer to the applicable 
# agreement for further details.

# ACDS 15.0 145 win32 2017.05.17.13:27:09

# ----------------------------------------
# Auto-generated simulation script

# ----------------------------------------
# Initialize variables
if ![info exists SYSTEM_INSTANCE_NAME] { 
  set SYSTEM_INSTANCE_NAME ""
} elseif { ![ string match "" $SYSTEM_INSTANCE_NAME ] } { 
  set SYSTEM_INSTANCE_NAME "/$SYSTEM_INSTANCE_NAME"
}

if ![info exists TOP_LEVEL_NAME] { 
  set TOP_LEVEL_NAME "NIOS_II_COPROCESSOR_MINIMIND"
}

if ![info exists QSYS_SIMDIR] { 
  set QSYS_SIMDIR "./../"
}

if ![info exists QUARTUS_INSTALL_DIR] { 
  set QUARTUS_INSTALL_DIR "C:/altera/15.0/quartus/"
}

# ----------------------------------------
# Initialize simulation properties - DO NOT MODIFY!
set ELAB_OPTIONS ""
set SIM_OPTIONS ""
if ![ string match "*-64 vsim*" [ vsim -version ] ] {
} else {
}

# ----------------------------------------
# Copy ROM/RAM files to simulation directory
alias file_copy {
  echo "\[exec\] file_copy"
  file copy -force $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_onchip_memory2_0.hex ./
  file copy -force $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_ociram_default_contents.dat ./
  file copy -force $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_ociram_default_contents.hex ./
  file copy -force $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_ociram_default_contents.mif ./
  file copy -force $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_a.dat ./
  file copy -force $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_a.hex ./
  file copy -force $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_a.mif ./
  file copy -force $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_b.dat ./
  file copy -force $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_b.hex ./
  file copy -force $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_b.mif ./
}

# ----------------------------------------
# Create compilation libraries
proc ensure_lib { lib } { if ![file isdirectory $lib] { vlib $lib } }
ensure_lib          ./libraries/     
ensure_lib          ./libraries/work/
vmap       work     ./libraries/work/
vmap       work_lib ./libraries/work/
if ![ string match "*ModelSim ALTERA*" [ vsim -version ] ] {
  ensure_lib                        ./libraries/altera_ver/            
  vmap       altera_ver             ./libraries/altera_ver/            
  ensure_lib                        ./libraries/lpm_ver/               
  vmap       lpm_ver                ./libraries/lpm_ver/               
  ensure_lib                        ./libraries/sgate_ver/             
  vmap       sgate_ver              ./libraries/sgate_ver/             
  ensure_lib                        ./libraries/altera_mf_ver/         
  vmap       altera_mf_ver          ./libraries/altera_mf_ver/         
  ensure_lib                        ./libraries/altera_lnsim_ver/      
  vmap       altera_lnsim_ver       ./libraries/altera_lnsim_ver/      
  ensure_lib                        ./libraries/cycloneiv_hssi_ver/    
  vmap       cycloneiv_hssi_ver     ./libraries/cycloneiv_hssi_ver/    
  ensure_lib                        ./libraries/cycloneiv_pcie_hip_ver/
  vmap       cycloneiv_pcie_hip_ver ./libraries/cycloneiv_pcie_hip_ver/
  ensure_lib                        ./libraries/cycloneiv_ver/         
  vmap       cycloneiv_ver          ./libraries/cycloneiv_ver/         
  ensure_lib                        ./libraries/altera/                
  vmap       altera                 ./libraries/altera/                
  ensure_lib                        ./libraries/lpm/                   
  vmap       lpm                    ./libraries/lpm/                   
  ensure_lib                        ./libraries/sgate/                 
  vmap       sgate                  ./libraries/sgate/                 
  ensure_lib                        ./libraries/altera_mf/             
  vmap       altera_mf              ./libraries/altera_mf/             
  ensure_lib                        ./libraries/altera_lnsim/          
  vmap       altera_lnsim           ./libraries/altera_lnsim/          
  ensure_lib                        ./libraries/cycloneiv_hssi/        
  vmap       cycloneiv_hssi         ./libraries/cycloneiv_hssi/        
  ensure_lib                        ./libraries/cycloneiv_pcie_hip/    
  vmap       cycloneiv_pcie_hip     ./libraries/cycloneiv_pcie_hip/    
  ensure_lib                        ./libraries/cycloneiv/             
  vmap       cycloneiv              ./libraries/cycloneiv/             
}
ensure_lib                                              ./libraries/error_adapter_0/                             
vmap       error_adapter_0                              ./libraries/error_adapter_0/                             
ensure_lib                                              ./libraries/avalon_st_adapter/                           
vmap       avalon_st_adapter                            ./libraries/avalon_st_adapter/                           
ensure_lib                                              ./libraries/rsp_mux_001/                                 
vmap       rsp_mux_001                                  ./libraries/rsp_mux_001/                                 
ensure_lib                                              ./libraries/rsp_mux/                                     
vmap       rsp_mux                                      ./libraries/rsp_mux/                                     
ensure_lib                                              ./libraries/rsp_demux/                                   
vmap       rsp_demux                                    ./libraries/rsp_demux/                                   
ensure_lib                                              ./libraries/cmd_mux_002/                                 
vmap       cmd_mux_002                                  ./libraries/cmd_mux_002/                                 
ensure_lib                                              ./libraries/cmd_mux/                                     
vmap       cmd_mux                                      ./libraries/cmd_mux/                                     
ensure_lib                                              ./libraries/cmd_demux_001/                               
vmap       cmd_demux_001                                ./libraries/cmd_demux_001/                               
ensure_lib                                              ./libraries/cmd_demux/                                   
vmap       cmd_demux                                    ./libraries/cmd_demux/                                   
ensure_lib                                              ./libraries/router_004/                                  
vmap       router_004                                   ./libraries/router_004/                                  
ensure_lib                                              ./libraries/router_002/                                  
vmap       router_002                                   ./libraries/router_002/                                  
ensure_lib                                              ./libraries/router_001/                                  
vmap       router_001                                   ./libraries/router_001/                                  
ensure_lib                                              ./libraries/router/                                      
vmap       router                                       ./libraries/router/                                      
ensure_lib                                              ./libraries/jtag_uart_0_avalon_jtag_slave_agent_rsp_fifo/
vmap       jtag_uart_0_avalon_jtag_slave_agent_rsp_fifo ./libraries/jtag_uart_0_avalon_jtag_slave_agent_rsp_fifo/
ensure_lib                                              ./libraries/jtag_uart_0_avalon_jtag_slave_agent/         
vmap       jtag_uart_0_avalon_jtag_slave_agent          ./libraries/jtag_uart_0_avalon_jtag_slave_agent/         
ensure_lib                                              ./libraries/nios2_qsys_0_data_master_agent/              
vmap       nios2_qsys_0_data_master_agent               ./libraries/nios2_qsys_0_data_master_agent/              
ensure_lib                                              ./libraries/jtag_uart_0_avalon_jtag_slave_translator/    
vmap       jtag_uart_0_avalon_jtag_slave_translator     ./libraries/jtag_uart_0_avalon_jtag_slave_translator/    
ensure_lib                                              ./libraries/nios2_qsys_0_data_master_translator/         
vmap       nios2_qsys_0_data_master_translator          ./libraries/nios2_qsys_0_data_master_translator/         
ensure_lib                                              ./libraries/rst_controller/                              
vmap       rst_controller                               ./libraries/rst_controller/                              
ensure_lib                                              ./libraries/irq_mapper/                                  
vmap       irq_mapper                                   ./libraries/irq_mapper/                                  
ensure_lib                                              ./libraries/mm_interconnect_0/                           
vmap       mm_interconnect_0                            ./libraries/mm_interconnect_0/                           
ensure_lib                                              ./libraries/onchip_memory2_0/                            
vmap       onchip_memory2_0                             ./libraries/onchip_memory2_0/                            
ensure_lib                                              ./libraries/nios2_qsys_0/                                
vmap       nios2_qsys_0                                 ./libraries/nios2_qsys_0/                                
ensure_lib                                              ./libraries/jtag_uart_0/                                 
vmap       jtag_uart_0                                  ./libraries/jtag_uart_0/                                 
ensure_lib                                              ./libraries/MINI_MIND_NEW_COMPONENT_V2_0/                
vmap       MINI_MIND_NEW_COMPONENT_V2_0                 ./libraries/MINI_MIND_NEW_COMPONENT_V2_0/                

# ----------------------------------------
# Compile device library files
alias dev_com {
  echo "\[exec\] dev_com"
  if ![ string match "*ModelSim ALTERA*" [ vsim -version ] ] {
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.v"               -work altera_ver            
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/220model.v"                        -work lpm_ver               
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.v"                           -work sgate_ver             
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.v"                       -work altera_mf_ver         
    vlog -sv "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/altera_lnsim_for_vhdl.sv"   -work altera_lnsim_ver      
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_hssi_atoms.v"            -work cycloneiv_hssi_ver    
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_pcie_hip_atoms.v"        -work cycloneiv_pcie_hip_ver
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_atoms.v"                 -work cycloneiv_ver         
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_syn_attributes.vhd"         -work altera                
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_standard_functions.vhd"     -work altera                
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/alt_dspbuilder_package.vhd"        -work altera                
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_europa_support_lib.vhd"     -work altera                
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives_components.vhd"  -work altera                
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.vhd"             -work altera                
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/220pack.vhd"                       -work lpm                   
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/220model.vhd"                      -work lpm                   
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate_pack.vhd"                    -work sgate                 
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.vhd"                         -work sgate                 
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf_components.vhd"          -work altera_mf             
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.vhd"                     -work altera_mf             
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim_components.vhd"       -work altera_lnsim          
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_hssi_components.vhd"     -work cycloneiv_hssi        
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_hssi_atoms.vhd"          -work cycloneiv_hssi        
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_pcie_hip_components.vhd" -work cycloneiv_pcie_hip    
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_pcie_hip_atoms.vhd"      -work cycloneiv_pcie_hip    
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_atoms.vhd"               -work cycloneiv             
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_components.vhd"          -work cycloneiv             
  }
}

# ----------------------------------------
# Compile the design files in correct order
alias com {
  echo "\[exec\] com"
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_avalon_st_adapter_error_adapter_0.vho"                      -work error_adapter_0                             
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_avalon_st_adapter.vhd"                                      -work avalon_st_adapter                           
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_rsp_mux_001.vho"                                            -work rsp_mux_001                                 
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_rsp_mux.vho"                                                -work rsp_mux                                     
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_rsp_demux.vho"                                              -work rsp_demux                                   
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_cmd_mux_002.vho"                                            -work cmd_mux_002                                 
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_cmd_mux.vho"                                                -work cmd_mux                                     
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_cmd_demux_001.vho"                                          -work cmd_demux_001                               
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_cmd_demux.vho"                                              -work cmd_demux                                   
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_router_004.vho"                                             -work router_004                                  
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_router_002.vho"                                             -work router_002                                  
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_router_001.vho"                                             -work router_001                                  
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_router.vho"                                                 -work router                                      
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_jtag_uart_0_avalon_jtag_slave_agent_rsp_fifo.vho"           -work jtag_uart_0_avalon_jtag_slave_agent_rsp_fifo
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_slave_agent.sv"                                                                       -work jtag_uart_0_avalon_jtag_slave_agent         
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_burst_uncompressor.sv"                                                                -work jtag_uart_0_avalon_jtag_slave_agent         
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_master_agent.sv"                                                                      -work nios2_qsys_0_data_master_agent              
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_slave_translator.sv"                                                                  -work jtag_uart_0_avalon_jtag_slave_translator    
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_master_translator.sv"                                                                 -work nios2_qsys_0_data_master_translator         
  vlog     "$QSYS_SIMDIR/submodules/mentor/altera_reset_controller.v"                                                                          -work rst_controller                              
  vlog     "$QSYS_SIMDIR/submodules/mentor/altera_reset_synchronizer.v"                                                                        -work rst_controller                              
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_irq_mapper.vho"                                                               -work irq_mapper                                  
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0.vhd"                                                        -work mm_interconnect_0                           
  vcom     "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_jtag_uart_0_avalon_jtag_slave_translator.vhd"               -work mm_interconnect_0                           
  vcom     "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_mini_mind_new_component_v2_0_avalon_slave_0_translator.vhd" -work mm_interconnect_0                           
  vcom     "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_nios2_qsys_0_jtag_debug_module_translator.vhd"              -work mm_interconnect_0                           
  vcom     "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_onchip_memory2_0_s1_translator.vhd"                         -work mm_interconnect_0                           
  vcom     "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_nios2_qsys_0_data_master_translator.vhd"                    -work mm_interconnect_0                           
  vcom     "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_nios2_qsys_0_instruction_master_translator.vhd"             -work mm_interconnect_0                           
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_onchip_memory2_0.vhd"                                                         -work onchip_memory2_0                            
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0.vhd"                                                             -work nios2_qsys_0                                
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_jtag_debug_module_sysclk.vhd"                                    -work nios2_qsys_0                                
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_jtag_debug_module_tck.vhd"                                       -work nios2_qsys_0                                
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_jtag_debug_module_wrapper.vhd"                                   -work nios2_qsys_0                                
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_oci_test_bench.vhd"                                              -work nios2_qsys_0                                
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_test_bench.vhd"                                                  -work nios2_qsys_0                                
  vcom     "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_jtag_uart_0.vhd"                                                              -work jtag_uart_0                                 
  vcom     "$QSYS_SIMDIR/submodules/BASIC_UNIT_1.vhd"                                                                                          -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/BASIC_UNIT_2.vhd"                                                                                          -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/FULLY_1.vhd"                                                                                               -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/FULLY_2.vhd"                                                                                               -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/HT_UNIT_1.vhd"                                                                                             -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/INPUT_BUFFER.vhd"                                                                                          -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/LSTM_1.vhd"                                                                                                -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/LSTM_2.vhd"                                                                                                -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/MEMORY_CELL_1.vhd"                                                                                         -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/MINI_MIND.vhd"                                                                                             -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/MINI_MIND_AVALON.vhd"                                                                                      -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/MINI_MIND_AVALON_INTERFACE.vhd"                                                                            -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/OUTPUT_NIOSII.vhd"                                                                                         -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_1_HT_UNIT.vhd"                                                                                        -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_1_MEMORY_CELL_1.vhd"                                                                                  -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_1_MEMORY_CELL_2.vhd"                                                                                  -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_1_MUX.vhd"                                                                                            -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_1_Wh.vhd"                                                                                             -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_1_Wx.vhd"                                                                                             -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_2_HT_UNIT.vhd"                                                                                        -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_GATE_1_ADDER.vhd"                                                                                     -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_MAC.vhd"                                                                                              -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_OUTPUT_1.vhd"                                                                                         -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_RAM_B.vhd"                                                                                            -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_RAM_INPUT_BUFFER_1.vhd"                                                                               -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_RAM_Wh.vhd"                                                                                           -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_RAM_Wx.vhd"                                                                                           -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_REGISTER_1.vhd"                                                                                       -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_SEC_ADDER_LSTM1.vhd"                                                                                  -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_SEC_MULTI_1.vhd"                                                                                      -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/UNIT_Wh_F2.vhd"                                                                                            -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/adder_1.vhd"                                                                                               -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/adder_2.vhd"                                                                                               -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/logic_comparator_sigmoid.vhd"                                                                              -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/logic_comparator_tanh.vhd"                                                                                 -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/multi1.vhd"                                                                                                -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/multionly.vhd"                                                                                             -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/sigmoidvhdl.vhd"                                                                                           -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/submodules/tanhvhdl.vhd"                                                                                              -work MINI_MIND_NEW_COMPONENT_V2_0                
  vcom     "$QSYS_SIMDIR/NIOS_II_COPROCESSOR_MINIMIND.vhd"                                                                                                                                       
}

# ----------------------------------------
# Elaborate top level design
alias elab {
  echo "\[exec\] elab"
  eval vsim -t ps $ELAB_OPTIONS -L work -L work_lib -L error_adapter_0 -L avalon_st_adapter -L rsp_mux_001 -L rsp_mux -L rsp_demux -L cmd_mux_002 -L cmd_mux -L cmd_demux_001 -L cmd_demux -L router_004 -L router_002 -L router_001 -L router -L jtag_uart_0_avalon_jtag_slave_agent_rsp_fifo -L jtag_uart_0_avalon_jtag_slave_agent -L nios2_qsys_0_data_master_agent -L jtag_uart_0_avalon_jtag_slave_translator -L nios2_qsys_0_data_master_translator -L rst_controller -L irq_mapper -L mm_interconnect_0 -L onchip_memory2_0 -L nios2_qsys_0 -L jtag_uart_0 -L MINI_MIND_NEW_COMPONENT_V2_0 -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cycloneiv_hssi_ver -L cycloneiv_pcie_hip_ver -L cycloneiv_ver -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cycloneiv_hssi -L cycloneiv_pcie_hip -L cycloneiv $TOP_LEVEL_NAME
}

# ----------------------------------------
# Elaborate the top level design with novopt option
alias elab_debug {
  echo "\[exec\] elab_debug"
  eval vsim -novopt -t ps $ELAB_OPTIONS -L work -L work_lib -L error_adapter_0 -L avalon_st_adapter -L rsp_mux_001 -L rsp_mux -L rsp_demux -L cmd_mux_002 -L cmd_mux -L cmd_demux_001 -L cmd_demux -L router_004 -L router_002 -L router_001 -L router -L jtag_uart_0_avalon_jtag_slave_agent_rsp_fifo -L jtag_uart_0_avalon_jtag_slave_agent -L nios2_qsys_0_data_master_agent -L jtag_uart_0_avalon_jtag_slave_translator -L nios2_qsys_0_data_master_translator -L rst_controller -L irq_mapper -L mm_interconnect_0 -L onchip_memory2_0 -L nios2_qsys_0 -L jtag_uart_0 -L MINI_MIND_NEW_COMPONENT_V2_0 -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cycloneiv_hssi_ver -L cycloneiv_pcie_hip_ver -L cycloneiv_ver -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cycloneiv_hssi -L cycloneiv_pcie_hip -L cycloneiv $TOP_LEVEL_NAME
}

# ----------------------------------------
# Compile all the design files and elaborate the top level design
alias ld "
  dev_com
  com
  elab
"

# ----------------------------------------
# Compile all the design files and elaborate the top level design with -novopt
alias ld_debug "
  dev_com
  com
  elab_debug
"

# ----------------------------------------
# Print out user commmand line aliases
alias h {
  echo "List Of Command Line Aliases"
  echo
  echo "file_copy                     -- Copy ROM/RAM files to simulation directory"
  echo
  echo "dev_com                       -- Compile device library files"
  echo
  echo "com                           -- Compile the design files in correct order"
  echo
  echo "elab                          -- Elaborate top level design"
  echo
  echo "elab_debug                    -- Elaborate the top level design with novopt option"
  echo
  echo "ld                            -- Compile all the design files and elaborate the top level design"
  echo
  echo "ld_debug                      -- Compile all the design files and elaborate the top level design with -novopt"
  echo
  echo 
  echo
  echo "List Of Variables"
  echo
  echo "TOP_LEVEL_NAME                -- Top level module name."
  echo
  echo "SYSTEM_INSTANCE_NAME          -- Instantiated system module name inside top level module."
  echo
  echo "QSYS_SIMDIR                   -- Qsys base simulation directory."
  echo
  echo "QUARTUS_INSTALL_DIR           -- Quartus installation directory."
}
file_copy
h
