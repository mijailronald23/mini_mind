
# (C) 2001-2017 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and 
# other software and tools, and its AMPP partner logic functions, and 
# any output files any of the foregoing (including device programming 
# or simulation files), and any associated documentation or information 
# are expressly subject to the terms and conditions of the Altera 
# Program License Subscription Agreement, Altera MegaCore Function 
# License Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by Altera 
# or its authorized distributors. Please refer to the applicable 
# agreement for further details.

# ACDS 15.0 145 win32 2017.05.17.13:27:09

# ----------------------------------------
# vcsmx - auto-generated simulation script

# ----------------------------------------
# initialize variables
TOP_LEVEL_NAME="NIOS_II_COPROCESSOR_MINIMIND"
QSYS_SIMDIR="./../../"
QUARTUS_INSTALL_DIR="C:/altera/15.0/quartus/"
SKIP_FILE_COPY=0
SKIP_DEV_COM=0
SKIP_COM=0
SKIP_ELAB=0
SKIP_SIM=0
USER_DEFINED_ELAB_OPTIONS=""
USER_DEFINED_SIM_OPTIONS="+vcs+finish+100"

# ----------------------------------------
# overwrite variables - DO NOT MODIFY!
# This block evaluates each command line argument, typically used for 
# overwriting variables. An example usage:
#   sh <simulator>_setup.sh SKIP_ELAB=1 SKIP_SIM=1
for expression in "$@"; do
  eval $expression
  if [ $? -ne 0 ]; then
    echo "Error: This command line argument, \"$expression\", is/has an invalid expression." >&2
    exit $?
  fi
done

# ----------------------------------------
# initialize simulation properties - DO NOT MODIFY!
ELAB_OPTIONS=""
SIM_OPTIONS=""
if [[ `vcs -platform` != *"amd64"* ]]; then
  :
else
  :
fi

# ----------------------------------------
# create compilation libraries
mkdir -p ./libraries/work/
mkdir -p ./libraries/error_adapter_0/
mkdir -p ./libraries/avalon_st_adapter/
mkdir -p ./libraries/rsp_mux_001/
mkdir -p ./libraries/rsp_mux/
mkdir -p ./libraries/rsp_demux/
mkdir -p ./libraries/cmd_mux_002/
mkdir -p ./libraries/cmd_mux/
mkdir -p ./libraries/cmd_demux_001/
mkdir -p ./libraries/cmd_demux/
mkdir -p ./libraries/router_004/
mkdir -p ./libraries/router_002/
mkdir -p ./libraries/router_001/
mkdir -p ./libraries/router/
mkdir -p ./libraries/jtag_uart_0_avalon_jtag_slave_agent_rsp_fifo/
mkdir -p ./libraries/irq_mapper/
mkdir -p ./libraries/mm_interconnect_0/
mkdir -p ./libraries/onchip_memory2_0/
mkdir -p ./libraries/nios2_qsys_0/
mkdir -p ./libraries/jtag_uart_0/
mkdir -p ./libraries/MINI_MIND_NEW_COMPONENT_V2_0/
mkdir -p ./libraries/altera/
mkdir -p ./libraries/lpm/
mkdir -p ./libraries/sgate/
mkdir -p ./libraries/altera_mf/
mkdir -p ./libraries/altera_lnsim/
mkdir -p ./libraries/cycloneiv_hssi/
mkdir -p ./libraries/cycloneiv_pcie_hip/
mkdir -p ./libraries/cycloneiv/

# ----------------------------------------
# copy RAM/ROM files to simulation directory
if [ $SKIP_FILE_COPY -eq 0 ]; then
  cp -f $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_onchip_memory2_0.hex ./
  cp -f $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_ociram_default_contents.dat ./
  cp -f $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_ociram_default_contents.hex ./
  cp -f $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_ociram_default_contents.mif ./
  cp -f $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_a.dat ./
  cp -f $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_a.hex ./
  cp -f $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_a.mif ./
  cp -f $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_b.dat ./
  cp -f $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_b.hex ./
  cp -f $QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_rf_ram_b.mif ./
fi

# ----------------------------------------
# compile device library files
if [ $SKIP_DEV_COM -eq 0 ]; then
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_syn_attributes.vhd"         -work altera            
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_standard_functions.vhd"     -work altera            
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/alt_dspbuilder_package.vhd"        -work altera            
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_europa_support_lib.vhd"     -work altera            
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives_components.vhd"  -work altera            
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.vhd"             -work altera            
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/220pack.vhd"                       -work lpm               
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/220model.vhd"                      -work lpm               
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate_pack.vhd"                    -work sgate             
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.vhd"                         -work sgate             
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf_components.vhd"          -work altera_mf         
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.vhd"                     -work altera_mf         
  vlogan +v2k -sverilog "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim.sv"                   -work altera_lnsim      
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim_components.vhd"       -work altera_lnsim      
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_hssi_components.vhd"     -work cycloneiv_hssi    
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_hssi_atoms.vhd"          -work cycloneiv_hssi    
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_pcie_hip_components.vhd" -work cycloneiv_pcie_hip
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_pcie_hip_atoms.vhd"      -work cycloneiv_pcie_hip
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_atoms.vhd"               -work cycloneiv         
  vhdlan                "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneiv_components.vhd"          -work cycloneiv         
fi

# ----------------------------------------
# compile design files in correct order
if [ $SKIP_COM -eq 0 ]; then
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_avalon_st_adapter_error_adapter_0.vho"                      -work error_adapter_0                             
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_avalon_st_adapter.vhd"                                      -work avalon_st_adapter                           
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_rsp_mux_001.vho"                                            -work rsp_mux_001                                 
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_rsp_mux.vho"                                                -work rsp_mux                                     
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_rsp_demux.vho"                                              -work rsp_demux                                   
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_cmd_mux_002.vho"                                            -work cmd_mux_002                                 
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_cmd_mux.vho"                                                -work cmd_mux                                     
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_cmd_demux_001.vho"                                          -work cmd_demux_001                               
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_cmd_demux.vho"                                              -work cmd_demux                                   
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_router_004.vho"                                             -work router_004                                  
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_router_002.vho"                                             -work router_002                                  
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_router_001.vho"                                             -work router_001                                  
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_router.vho"                                                 -work router                                      
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0_jtag_uart_0_avalon_jtag_slave_agent_rsp_fifo.vho"           -work jtag_uart_0_avalon_jtag_slave_agent_rsp_fifo
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_irq_mapper.vho"                                                               -work irq_mapper                                  
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_mm_interconnect_0.vhd"                                                        -work mm_interconnect_0                           
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_jtag_uart_0_avalon_jtag_slave_translator.vhd"               -work mm_interconnect_0                           
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_mini_mind_new_component_v2_0_avalon_slave_0_translator.vhd" -work mm_interconnect_0                           
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_nios2_qsys_0_jtag_debug_module_translator.vhd"              -work mm_interconnect_0                           
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_onchip_memory2_0_s1_translator.vhd"                         -work mm_interconnect_0                           
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_nios2_qsys_0_data_master_translator.vhd"                    -work mm_interconnect_0                           
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/nios_ii_coprocessor_minimind_mm_interconnect_0_nios2_qsys_0_instruction_master_translator.vhd"             -work mm_interconnect_0                           
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_onchip_memory2_0.vhd"                                                         -work onchip_memory2_0                            
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0.vhd"                                                             -work nios2_qsys_0                                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_jtag_debug_module_sysclk.vhd"                                    -work nios2_qsys_0                                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_jtag_debug_module_tck.vhd"                                       -work nios2_qsys_0                                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_jtag_debug_module_wrapper.vhd"                                   -work nios2_qsys_0                                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_oci_test_bench.vhd"                                              -work nios2_qsys_0                                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_nios2_qsys_0_test_bench.vhd"                                                  -work nios2_qsys_0                                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/NIOS_II_COPROCESSOR_MINIMIND_jtag_uart_0.vhd"                                                              -work jtag_uart_0                                 
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/BASIC_UNIT_1.vhd"                                                                                          -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/BASIC_UNIT_2.vhd"                                                                                          -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/FULLY_1.vhd"                                                                                               -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/FULLY_2.vhd"                                                                                               -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/HT_UNIT_1.vhd"                                                                                             -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/INPUT_BUFFER.vhd"                                                                                          -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/LSTM_1.vhd"                                                                                                -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/LSTM_2.vhd"                                                                                                -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/MEMORY_CELL_1.vhd"                                                                                         -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/MINI_MIND.vhd"                                                                                             -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/MINI_MIND_AVALON.vhd"                                                                                      -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/MINI_MIND_AVALON_INTERFACE.vhd"                                                                            -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/OUTPUT_NIOSII.vhd"                                                                                         -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_1_HT_UNIT.vhd"                                                                                        -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_1_MEMORY_CELL_1.vhd"                                                                                  -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_1_MEMORY_CELL_2.vhd"                                                                                  -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_1_MUX.vhd"                                                                                            -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_1_Wh.vhd"                                                                                             -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_1_Wx.vhd"                                                                                             -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_2_HT_UNIT.vhd"                                                                                        -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_GATE_1_ADDER.vhd"                                                                                     -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_MAC.vhd"                                                                                              -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_OUTPUT_1.vhd"                                                                                         -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_RAM_B.vhd"                                                                                            -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_RAM_INPUT_BUFFER_1.vhd"                                                                               -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_RAM_Wh.vhd"                                                                                           -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_RAM_Wx.vhd"                                                                                           -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_REGISTER_1.vhd"                                                                                       -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_SEC_ADDER_LSTM1.vhd"                                                                                  -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_SEC_MULTI_1.vhd"                                                                                      -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/UNIT_Wh_F2.vhd"                                                                                            -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/adder_1.vhd"                                                                                               -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/adder_2.vhd"                                                                                               -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/logic_comparator_sigmoid.vhd"                                                                              -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/logic_comparator_tanh.vhd"                                                                                 -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/multi1.vhd"                                                                                                -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/multionly.vhd"                                                                                             -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/sigmoidvhdl.vhd"                                                                                           -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/submodules/tanhvhdl.vhd"                                                                                              -work MINI_MIND_NEW_COMPONENT_V2_0                
  vhdlan -xlrm "$QSYS_SIMDIR/NIOS_II_COPROCESSOR_MINIMIND.vhd"                                                                                                                                       
fi

# ----------------------------------------
# elaborate top level design
if [ $SKIP_ELAB -eq 0 ]; then
  vcs -lca -t ps $ELAB_OPTIONS $USER_DEFINED_ELAB_OPTIONS $TOP_LEVEL_NAME
fi

# ----------------------------------------
# simulate
if [ $SKIP_SIM -eq 0 ]; then
  ./simv $SIM_OPTIONS $USER_DEFINED_SIM_OPTIONS
fi
