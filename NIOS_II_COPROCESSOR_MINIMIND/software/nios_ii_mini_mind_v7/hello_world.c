#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> ///sleep

#include <sys/alt_stdio.h>
#include <system.h>
#include <io.h>

#include <time.h>

void mini_mind_reset()
	{

	  	  int no_write = 2;
	  	  no_write = no_write << 23;
	  	  IOWR_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0,no_write);  /*NO WRITE*/
	  	  usleep(1);

	  	  int reset = 3;
	  	  reset = reset << 23;
	  	  IOWR_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0,reset);
	  	  usleep(400);

	  	  IOWR_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0,no_write);  /*NO WRITE*/
	  	  usleep(1);

	}



void mini_mind_write(int write_vector[])
	{
		  int b=0;
	  	  int write_var=0;
	  	  int address_1=0;
	  	  int data_bus_avalon=0;
	  	  for( b = 0; b < 128; b ++ )
	  	  	  {
	  		  	  	write_var=0;
	  		  	    address_1=0;
	  		  	    data_bus_avalon=0;
	  		  	  	address_1=1; 				/*instruction write memory */
	  		  	  	address_1=address_1 << 7; 	/*instruction address assign */
	  		  	    write_var = address_1 +b;   /*instruction address memory*/
	  		  	    write_var = write_var << 16;   /*instruction data assign*/
	  		  	    data_bus_avalon =write_vector[b]+ write_var;
	  		  	    //printf("write_vector[b] = %d\n",write_vector[b]);
	  		  	    IOWR_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0,data_bus_avalon);
	  		  	    usleep(1);
	  	  	  }
	  	  write_var=0;
	  	  address_1=0;
	  	  data_bus_avalon=0;
	  	  address_1=1; 				/*instruction write memory */
	  	  address_1=address_1 << 7; 	/*instruction address assign */
	  	  write_var = address_1 ;   /*instruction address memory*/
	  	  write_var = write_var << 16;   /*instruction data assign*/
	  	  data_bus_avalon =write_vector[0]+ write_var;
	  	  IOWR_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0,data_bus_avalon);
	  	  usleep(1);

	}

void mini_mind_read ()
	{
	  	  int no_write = 2;
	  	  no_write = no_write << 23;
	  	  IOWR_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0,no_write);  /*NO WRITE*/

	  	  int enable = 4;
	  	  enable = enable << 23;
	  	  IOWR_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0,enable);   /*ENABLE MINI MIND*/
	  	  usleep(120000);											 /*DELAY OF MINI MIND */
	  	  IORD_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0);
	  	  IORD_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0);
	}

void mini_mind_load_weights ()
	{
		  int a1,a;
		  int value_w;
		  int size_w;
		  int write_1;
		  int write_2;
		  int write_3;
		  FILE* write_values_memory=NULL;
		  write_values_memory = fopen("/mnt/host/memory_1_mini_mind.txt","r");
		  int var_init_memory [29]={0,16384,512,128,16384,512,128,16384,512,128,16384,512,128,16384,16384,128,16384,16384,128,16384,16384,128,16384,16384,128,16384,128,16384,128};
		  int erase_1= 7;
		  erase_1 = erase_1 <<23 ;
		  for( a1 = 1; a1 < 30; a1 ++ )
		  	  	  {
			  		size_w = var_init_memory [a1];
			  		write_1 = a1;
			  		write_1 = write_1 << 3;
			  		write_2 = write_1+6;
			  		write_2 = write_2 << 7;
			  		write_2 = write_2+1;
			  		write_2 = write_2<< 16;
			  		printf("memory_value= %d\n",a1);
			  		printf("value= %d\n",var_init_memory [a1]);

			  		for( a = 0; a < size_w; a ++ )
		    	  	    {

		    	 	  	  fscanf(write_values_memory,"%d",&value_w);
			  		  	  write_3 = write_2+value_w;
		  	 	  	  	  IOWR_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0,write_3);

		    	  	  	 }

			  		IOWR_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0,erase_1);
			  		usleep(1);
		  	  	  }
		  fclose(write_values_memory);
		  mini_mind_reset();
	}

int main()
{
	printf("Hello from Nios_MINI_MIND_PROJECT!!!!!\n");
	int a1,a;
	int gestures = 10;
	int a_fin = 128;
	int var_mini_mind [128];

	mini_mind_load_weights();/*LOAD WEIGHTS  */

	FILE* test_gestures_nios=NULL;
	test_gestures_nios = fopen("/mnt/host/test_gestures.txt","r");
	FILE* test_save_txt=NULL;
	test_save_txt = fopen("/mnt/host/test_save_txt.txt","w");
	for( a1 = 0; a1 < gestures; a1 ++ )
	      {
	    	printf("GESTURE N = %d\n",a1);
	    	for( a = 0; a < a_fin; a ++ )
	    	  	    {
	    	 	  	  fscanf(test_gestures_nios,"%d",&var_mini_mind[a]);
	    	  	  	}

	    	mini_mind_write(var_mini_mind);
	    	mini_mind_read();
	    	for( a = 0; a < 128; a ++ )
	    	  	  	{
	    	  		  fprintf(test_save_txt," %d\n",IORD_32DIRECT(MINI_MIND_NEW_COMPONENT_V2_0_BASE,0));
	    	  	  	}

	    	mini_mind_reset();
	      }
	fclose(test_gestures_nios);
	fclose(test_save_txt);
	printf("\n READN AND WROTE ALL FILES SUCCESFULLY\n");
	return 0;
}
