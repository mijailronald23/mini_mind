library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

entity INPUT_BUFFER is   
  port(
		clk_INPUT_BUFFER 			  : IN STD_LOGIC  ;
		reset_INPUT_BUFFER	 	  : IN STD_LOGIC  ;
		input_nios_bus_data 		  : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		address_nios_ii 		 	  : IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		address_write  	 		  : IN STD_LOGIC  ;
		enable_MINI_MIND 		     : IN STD_LOGIC  ;	
		result_INPUT_BUFFER       : OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end INPUT_BUFFER;


architecture Behavioral of INPUT_BUFFER is 



component UNIT_RAM_INPUT_BUFFER_1
port(
		address		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		clock		: IN STD_LOGIC  ;
		data		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		rden		: IN STD_LOGIC  ;
		wren		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;


signal address_ram 			 : STD_LOGIC_VECTOR (6 DOWNTO 0)  :="1111111";
signal signal_ram1	 		 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal clear_1	 				 : STD_LOGIC  :='0';
signal mac_enable	 			 : STD_LOGIC  :='0';
signal read_memory_enable	 : STD_LOGIC  :='0';
signal counter		 			 : STD_LOGIC_VECTOR (2 DOWNTO 0)  :=(OTHERS => '0');
signal counter_address      : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal extra      : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal move_address_1		 : STD_LOGIC_VECTOR (6 DOWNTO 0)  :="1111111";
signal counter_add	 			 : STD_LOGIC_VECTOR (2 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_1	 			 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_1	 : STD_LOGIC :='0';
signal ram_init_1	 : STD_LOGIC  :='0';


begin



RAM_INPUT_BUFFER_1 : UNIT_RAM_INPUT_BUFFER_1 port map(
						address	=>address_ram,
						clock		=>clk_INPUT_BUFFER,
						data		=>input_nios_bus_data,
						rden		=>read_memory_enable,
						wren		=>address_write,
						q			=>result_INPUT_BUFFER);
						

process (clk_INPUT_BUFFER,enable_MINI_MIND)
begin  
if(clk_INPUT_BUFFER'event and clk_INPUT_BUFFER='1') then

				if (reset_INPUT_BUFFER = '1') then
					read_memory_enable <= '0';							 ---
					counter <=(OTHERS => '0');							 ---
					extra  <= (OTHERS => '0');							 ---
					counter_add<= (OTHERS => '0');					 ---
					delay_Ht_1 <= (OTHERS => '0');					 ---
					counter_address  <= (OTHERS => '0');          ---
					delay_1  <= '0';										 ---
					move_address_1	<= "1111111";	
					address_ram <= "1111111";							
				end if;
				if (enable_MINI_MIND = '1') then 
						if ( counter_address <= "01111111") then	
								if ( delay_1 = '1') then  --4
										if ( counter> "011") then  --4
												if ( extra= "10000000") then  --DELAY CLEAR MAC - 128
															counter <=(OTHERS => '0');
															extra  <= (OTHERS => '0');
															counter_add<= (OTHERS => '0');
															counter_address <= counter_address +1;
															address_ram <= move_address_1;			------------------
												
												else	
															extra <= extra + 1;
			
												end if;
										else	
				
												counter <= counter +1;
												address_ram <= address_ram +1;
												read_memory_enable <= '1';		
												
										end if;
								else 
										delay_1 <='1';
								end if;
						end  if;
				end if;
					
				if (enable_MINI_MIND = '0') then 
						read_memory_enable <= '0';
						address_ram <= address_nios_ii;
				end if;
				if ( counter_address > "01111111") then
						counter <=(OTHERS => '0');
						extra  <= (OTHERS => '0');
						if (delay_Ht_1 > "11000") then
									delay_Ht_1 <= (OTHERS => '0');
									counter_address  <= (OTHERS => '0');
									
						else 
									delay_Ht_1 <= delay_Ht_1 +1;
									
						end if;
						if (counter_add < "100") then
									counter_add <= counter_add +1;
									move_address_1 <= move_address_1+1;
									address_ram  <= address_ram+1;
						end if;	
				end if;	
					


end if;
end process;






end Behavioral;