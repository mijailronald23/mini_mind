library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

entity FULLY_2 is
  port(
		clk_FULLY_2 	 					: IN STD_LOGIC  ;
		reset_FULLY_2 	 					: IN STD_LOGIC  ;
		enable_FULLY_2 					: IN STD_LOGIC  ;
		data_FULLY_2_Wh 					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		result_RELU_FULLY_2      		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		extra_output_fully_2		    	: OUT STD_LOGIC;                     
		--nios_ii 
		enable_write_memory				: IN STD_LOGIC  ;
		write_memory_select				: IN STD_LOGIC_VECTOR (1	DOWNTO 0);
		dataa_memory_W  					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_W  				: IN STD_LOGIC_VECTOR (13 DOWNTO 0));
end FULLY_2;


architecture Behavioral of FULLY_2 is 


----UNIT_Wh_F2
component UNIT_Wh_F2                         
  port(
		clk_UNIT_Wh_F2 			: IN STD_LOGIC  ;
		reset_UNIT_Wh_F2 			: IN STD_LOGIC  ;
		enable_UNIT_Wh_F2 		: IN STD_LOGIC  ;
		dataa_input_Wh				: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		signal_ready_Wh  		 	: OUT STD_LOGIC  ;
		--nios_ii 
		enable_write_memory		: IN STD_LOGIC  ;
		write_memory_1				: IN STD_LOGIC  ;
		dataa_memory_Wh  			: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_Wh 		: IN STD_LOGIC_VECTOR (13	DOWNTO 0);
		----		
		result_Wh        : OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;


----UNIT_B
component UNIT_RAM_B port(
		aclr		: IN STD_LOGIC  := '0';
		address	: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		data		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		rden		: IN STD_LOGIC  := '1';
		wren		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
end component;

----UNIT_ADDER
component UNIT_GATE_1_ADDER port(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
end component;


----UNIT_REGISTER
component UNIT_REGISTER_1 port(
		clock		: IN STD_LOGIC ;
		data		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		enable		: IN STD_LOGIC ;
		load		: IN STD_LOGIC ;
		sclr		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
end component;


signal result_adder_1		 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_adder_2		 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_memory_B		  			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_Wh		 		 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_Wh_1		 	 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_register_1 	 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal address_ram_1   		 			 : STD_LOGIC_VECTOR (6 DOWNTO 0)	  :="1111111"; --INIT VALUE OF ADDRESS 
signal counter_B     		 			 : STD_LOGIC_VECTOR (7 DOWNTO 0)	  :=(OTHERS => '0');
signal counter_register_output 		 : STD_LOGIC_VECTOR (7 DOWNTO 0)	  :=(OTHERS => '0');
signal counter_register_2 	 			 : STD_LOGIC_VECTOR (7 DOWNTO 0)	  :=(OTHERS => '0');
signal signal_ready_Wh_1 	 			 : STD_LOGIC 							  :='0';
signal enable_UNIT_Wh_F2_1  			 : STD_LOGIC 							  :='0';
signal init_bias            			 : STD_LOGIC 							  :='0';
signal enable_register		 			 : STD_LOGIC 							  :='0';
signal read_memory_B       			 : STD_LOGIC 							  :='0';
signal delay_Ht_1 		 				 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_2 		 				 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_3 						 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_1	 				 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_2	 				 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_3	 				 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal extra_result_RELU_FULLY_2	 	 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal reg_result_FULLY_2	 			 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal enable_HT_output_FULLY_2		 : STD_LOGIC;
signal counter_output_memory : STD_LOGIC_VECTOR (7 DOWNTO 0);
--nios ii variables 
signal data_input_memory	 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal write_memory_value   : STD_LOGIC  :='0';

begin



UNIT_GATE_1_Wh : UNIT_Wh_F2 port map(
							clk_UNIT_Wh_F2 			=>clk_FULLY_2,
							reset_UNIT_Wh_F2			=>reset_FULLY_2,
							enable_UNIT_Wh_F2 		=>enable_UNIT_Wh_F2_1,
							dataa_input_Wh				=>data_FULLY_2_Wh,
							signal_ready_Wh 			=>signal_ready_Wh_1,
							--nios_ii 
							enable_write_memory		=>enable_write_memory,
							write_memory_1				=>write_memory_select(0),
							dataa_memory_Wh  			=>dataa_memory_W,
							address_memory_Wh 		=>address_memory_W,
							---							
							result_Wh        			=>result_Wh);
							

							
							
UNIT_GATE_1_B 	: UNIT_RAM_B port map(
							aclr							=>reset_FULLY_2,
							address						=>address_ram_1,
							clock							=>clk_FULLY_2,
							data							=>data_input_memory,
							rden							=>read_memory_B,
							wren							=>write_memory_value,
							q								=>result_memory_B);
							

							
UNIT_GATE_ADDER_1 : UNIT_GATE_1_ADDER port map(
								clock						=>clk_FULLY_2,
								dataa						=>result_Wh_1,
								datab						=>result_memory_B,
								result					=>result_adder_1);

								
UNIT_REGISTER : 	UNIT_REGISTER_1 port map(
								clock						=>clk_FULLY_2,
								data						=>result_adder_1,
								enable					=>enable_register,
								load						=>enable_register,
								sclr						=>reset_FULLY_2,
								q							=>reg_result_FULLY_2);
							
result_RELU_FULLY_2 <=extra_result_RELU_FULLY_2;
extra_output_fully_2 <=enable_HT_output_FULLY_2;


--Syncronize Wh
enable_Wh :process (clk_FULLY_2,enable_FULLY_2)
begin  
	if(clk_FULLY_2'event and clk_FULLY_2='1') then
				if (enable_FULLY_2='1') then 
						enable_UNIT_Wh_F2_1 <= '1';
				end if;
				if (enable_FULLY_2='0') then
						enable_UNIT_Wh_F2_1 <= '0';
				end if;

	end if;
end process enable_Wh;


						
--ADDER-1-Wh
process_adder_Wh :process (clk_FULLY_2,signal_ready_Wh_1)
begin  
	if(clk_FULLY_2'event and clk_FULLY_2='1') then
				if (signal_ready_Wh_1 = '1') then 
						result_Wh_1 <= result_Wh;
				end if;
				if (signal_ready_Wh_1 = '0') then 
						result_Wh_1 <= (OTHERS => '0');
				end if;

	end if;
end process process_adder_Wh;




--Read B - values 
process (clk_FULLY_2,enable_UNIT_Wh_F2_1)
begin  
	if(clk_FULLY_2'event and clk_FULLY_2='1') then	
	
				if (enable_write_memory = '1') then 				--write memory unit
						address_ram_1				<= address_memory_W(6 DOWNTO 0);
						data_input_memory			<= dataa_memory_W;
						write_memory_value		<= write_memory_select(1);
				end if;
				if (reset_FULLY_2 ='1') then
						counter_B <= (OTHERS => '0');
						read_memory_B <= '0';
						address_ram_1 <= "1111111";
						delay_Ht_3 <= (OTHERS => '0');
						counter_128_Ht_3 <= (OTHERS => '0');
				end if;
				init_bias <= enable_UNIT_Wh_F2_1;
				if (init_bias = '1') then
						if (counter_B = "10000011") then    --128(Neurons)+ 3 cycles of delay
					
									read_memory_B <= '1';
									address_ram_1   <= address_ram_1 +1;
									counter_B <= "11111111";
									counter_128_Ht_3 <= counter_128_Ht_3 +1;
						else 
									read_memory_B <= '0';
									counter_B <= counter_B +1;						
						end if;
				
				end if;

				if ( counter_128_Ht_3 > "01111111") then
						counter_B <= "11111111";
						address_ram_1 <= "1111111";
						if (delay_Ht_3 > "11000") then  				
									counter_128_Ht_3 <= (OTHERS => '0');
									delay_Ht_3 <= (OTHERS => '0');
							
						else 
									delay_Ht_3 <= delay_Ht_3 +1;
							
						end if;
					
				end if;
				
				

	end if;
end process;




--enable register 
process (clk_FULLY_2,enable_FULLY_2)
begin  
	if(clk_FULLY_2'event and clk_FULLY_2='1') then	
				if (reset_FULLY_2 ='1') then
							counter_register_output <= (OTHERS => '0');
							enable_register <= '0';
							counter_128_Ht_2 <= (OTHERS => '0');
							delay_Ht_2 <= (OTHERS => '0');
				end if;
				if (enable_FULLY_2= '1') then
							if (counter_register_output > "10000111") then  --128(Neurons)+ x cycles of delay(MAC and MEMORY)
									counter_register_output <="00000100";
									enable_register <= '1';
									counter_128_Ht_2 <= counter_128_Ht_2 +1;
								
							else
									enable_register <= '0';
									counter_register_output <= counter_register_output +1;
						
							end if;
				
				end if;
				

				if ( counter_128_Ht_2 > "01111111") then
						counter_register_output <="00000100";
		
						if (delay_Ht_2 > "11000") then  				
									counter_128_Ht_2 <= (OTHERS => '0');
									delay_Ht_2 <= (OTHERS => '0');
							
						else 
									delay_Ht_2 <= delay_Ht_2 +1;
							
						end if;
					
				end if;				
				
				
end if;
end process;




--RELU-convert
relu_convert: process (clk_FULLY_2,enable_FULLY_2)
begin  
	if(clk_FULLY_2'event and clk_FULLY_2='1') then	
				if (enable_FULLY_2= '1') then
						if  (reg_result_FULLY_2(15)='1') then 
									extra_result_RELU_FULLY_2<=(OTHERS => '0');
						end if;
						if  (reg_result_FULLY_2(15)='0') then 
									extra_result_RELU_FULLY_2<=reg_result_FULLY_2;
						end if;
				
				end if;
				
	end if;
end process relu_convert;




--enable output
process (clk_FULLY_2,enable_FULLY_2)
begin  
	if(clk_FULLY_2'event and clk_FULLY_2='1') then	
	
				if (reset_FULLY_2 ='1') then
						counter_128_Ht_1 <= (OTHERS => '0');
						delay_Ht_1 <= (OTHERS => '0');
						counter_output_memory <= (OTHERS => '0');
						enable_HT_output_FULLY_2 <= '0';
				end if;
				if (enable_FULLY_2 ='1') then
						if  (counter_output_memory =  "10011100") then --128(Neurons)+ x cycles of delay(MAC and MEMORY and ADDERS 1-2) 
									enable_HT_output_FULLY_2 <= '1';
									counter_output_memory <="00011000";  --6 INIT value to continue with the cycle of 128
									counter_128_Ht_1 <= counter_128_Ht_1 +1;
								
						else
									enable_HT_output_FULLY_2 <= '0';
									counter_output_memory <= counter_output_memory +1;
						
						end if;
				
				end if;
				
				if ( counter_128_Ht_1 > "01111111") then
				   	counter_output_memory <="00011000";
						if (delay_Ht_1 > "11000") then  				
									counter_128_Ht_1     <= (OTHERS => '0');
									delay_Ht_1 <= (OTHERS => '0');
							
						else 
									delay_Ht_1 <= delay_Ht_1 +1;
							
						end if;
					
				end if;
				
				
			
				
	end if;
end process;



end Behavioral;