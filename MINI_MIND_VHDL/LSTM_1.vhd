library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

entity LSTM_1 is               
  port(
		clk_LSTM_1 	 						: IN STD_LOGIC  ;
		reset_LSTM_1					   : IN STD_LOGIC ;
		enable_LSTM_1 						: IN STD_LOGIC  ;		
		data_INPUT_LSTM_Wx				: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		--nios_ii 
		enable_write_memory				: IN STD_LOGIC  ;
		write_memory_select				: IN STD_LOGIC_VECTOR (11	DOWNTO 0);
		dataa_memory_W  					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_W  				: IN STD_LOGIC_VECTOR (13 DOWNTO 0);		 
		-------
		output_LSTM_1	      			: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end LSTM_1;

architecture Behavioral of LSTM_1 is 

---BASIC_UNIT_LSTM_1
component BASIC_UNIT_1 
  port(
 		clk_BASIC_UNIT_1 	 				: IN STD_LOGIC  ;
		reset_BASIC_UNIT_1 	 			: IN STD_LOGIC  ;
		enable_BASIC_UNIT_1 				: IN STD_LOGIC  ;
		data_BASIC_UNIT_1_Wh 			: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		data_BASIC_UNIT_1_Wx 			: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		enable_OUTPUT_BASIC_UNIT_1		: OUT STD_LOGIC  ;
		--nios_ii 
		enable_write_memory				: IN STD_LOGIC  ;
		write_memory_select				: IN STD_LOGIC_VECTOR (2	DOWNTO 0);
		dataa_memory_W  					: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_W  				: IN STD_LOGIC_VECTOR (13 DOWNTO 0);
		----
		result_BASIC_UNIT_1      		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;

---MEMORY-CELL-LSTM_1
component MEMORY_CELL_1 port(
		clk_MEMORY_CELL_1 	 			: IN STD_LOGIC  ;
		reset_MEMORY_CELL_1  		 	: IN STD_LOGIC  ;
		enable_MEMORY_CELL_1 			: IN STD_LOGIC  ;
		enable_WRITE_MEMORY_CELL_1 	: IN STD_LOGIC  ;
		data_INPUT_MEMORY_CELL_1 	 	: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		result_MEMORY_CELL_1 			: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;

---HT_UNIT_1
component HT_UNIT_1 port(
		clk_HT_UNIT_1 	 					: IN STD_LOGIC  ;
		reset_HT_UNIT_1		 	 		: IN STD_LOGIC  ;
		enable_HT_UNIT_1 					: IN STD_LOGIC  ;
		enable_WRITE_HT_UNIT_1 			: IN STD_LOGIC  ;
		data_INPUT_HT_UNIT_1 	 		: IN STD_LOGIC_VECTOR (15	DOWNTO 0);	
		result_HT_UNIT_1 				   : OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;


---MULTI-SECTION_1
component UNIT_SEC_MULTI_1 port(
		clock									: IN STD_LOGIC ;
		dataa									: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		datab									: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result								: OUT STD_LOGIC_VECTOR (31 DOWNTO 0));
end component;

---ADDER-SECTION_1
component UNIT_SEC_ADDER_LSTM1 port(
		clock									: IN STD_LOGIC ;
		dataa									: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		datab									: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		result								: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
end component;

---SIGMOID FUNCTION
component sigmoidvhdl port(
	   clk_sigmoid	      				: IN STD_LOGIC  ;
		reset_sigmoid	    				: IN STD_LOGIC  ;
		enable_sigmoid 	 				: IN STD_LOGIC  ;
		input_sigmoid		 				: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result_1           				: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;



---TANH FUNCTION
component tanhvhdl port(
	   clk_tanh  	      				: IN STD_LOGIC  ;
		reset_tanh	      				: IN STD_LOGIC  ;
		enable_tanh	   					: IN STD_LOGIC  ;
		input_tanh  						: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		extra_logic_result 				: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		result_1          				: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;


signal extra_enable_output_LSTM_1 		: STD_LOGIC ;
signal extra_enable_output_LSTM_2 		: STD_LOGIC ;
signal extra_enable_output_LSTM_3		: STD_LOGIC ;
signal extra_enable_output_LSTM_4 		: STD_LOGIC ;
signal extra_extra_logic_result1 		: STD_LOGIC_VECTOR (3 DOWNTO 0);
signal extra_extra_logic_result2 		: STD_LOGIC_VECTOR (3 DOWNTO 0);
signal result_UNIT_1		 			   	: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_UNIT_2		 			   	: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_UNIT_3		 			   	: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_UNIT_4		 			   	: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_multi_1		 				: STD_LOGIC_VECTOR (31 DOWNTO 0)  :=(OTHERS => '0');
signal result_multi_2		 				: STD_LOGIC_VECTOR (31 DOWNTO 0)  :=(OTHERS => '0');
signal result_multi_3		 				: STD_LOGIC_VECTOR (31 DOWNTO 0)  :=(OTHERS => '0');
signal result_memory_cell_1				: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal enable_section_output_memory		: STD_LOGIC  :='0';
signal result_sigmoid_1						: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_sigmoid_2						: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_sigmoid_3						: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_tanh_1							: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal result_adder_1_sec_LSTM_1_mod	: STD_LOGIC_VECTOR (31	DOWNTO 0) :=(OTHERS => '0');
signal result_adder_1_sec_LSTM_1 		: STD_LOGIC_VECTOR (15	DOWNTO 0) :=(OTHERS => '0');
signal result_tanh_sec_LSTM_1		 		: STD_LOGIC_VECTOR (15	DOWNTO 0) :=(OTHERS => '0');
signal data_INPUT_LSTM_Wh					: STD_LOGIC_VECTOR (15	DOWNTO 0);
signal counter_memory_cell					: STD_LOGIC_VECTOR (7 DOWNTO 0) :=(OTHERS => '0');
signal enable_write_memory_cell			: STD_LOGIC  :='0';
signal enable_HT_output_LSTM_1 			: STD_LOGIC  :='0';
signal counter_output_memory 				: STD_LOGIC_VECTOR (7 DOWNTO 0);
signal counter_tanh_1	 		 			: STD_LOGIC_VECTOR (7 DOWNTO 0)	  :=(OTHERS => '0');
signal enable_tanh_section 				: STD_LOGIC  :='0';
signal delay_Ht_1 		 					: STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_2 		 					: STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_1	 					: STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_2	 					: STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal output_multi_3_LSTM_1		   	: STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');


begin


---INPUT_GATE_LSTM_1
-----------------------------------------------------------------------------------------------------------------------
UNIT_1_INPUT_GATE_1 : BASIC_UNIT_1 port map(
								clk_BASIC_UNIT_1					=>clk_LSTM_1,
								reset_BASIC_UNIT_1 	 			=>reset_LSTM_1,
								enable_BASIC_UNIT_1 				=>enable_LSTM_1,
								data_BASIC_UNIT_1_Wh 			=>data_INPUT_LSTM_Wh,
								data_BASIC_UNIT_1_Wx 			=>data_INPUT_LSTM_Wx,
								enable_OUTPUT_BASIC_UNIT_1		=>extra_enable_output_LSTM_1,
								--nios_ii 
								enable_write_memory				=>enable_write_memory,
								write_memory_select				=>write_memory_select(2 DOWNTO 0),
								dataa_memory_W  					=>dataa_memory_W,
								address_memory_W  				=>address_memory_W,
								----
								result_BASIC_UNIT_1      		=>result_UNIT_1);
		

---SIGMOID FUNCTION
UNIT_1_SIGMOID_1	:	sigmoidvhdl port map(
								clk_sigmoid							=>clk_LSTM_1,
								reset_sigmoid						=>reset_LSTM_1,
								enable_sigmoid 					=>extra_enable_output_LSTM_1,
								input_sigmoid						=>result_UNIT_1,		
								result_1      						=>result_sigmoid_1);

---CANDIDATE_MEMORY_1
-----------------------------------------------------------------------------------------------------------------------
UNIT_2_CANDIDATE_MEMORY_1 : BASIC_UNIT_1 port map(
								clk_BASIC_UNIT_1					=>clk_LSTM_1,
								reset_BASIC_UNIT_1 	 			=>reset_LSTM_1,
								enable_BASIC_UNIT_1 				=>enable_LSTM_1,
								data_BASIC_UNIT_1_Wh 			=>data_INPUT_LSTM_Wh,
								data_BASIC_UNIT_1_Wx 			=>data_INPUT_LSTM_Wx,
								enable_OUTPUT_BASIC_UNIT_1		=>extra_enable_output_LSTM_2,
								--nios_ii 
								enable_write_memory				=>enable_write_memory,
								write_memory_select				=>write_memory_select(5 DOWNTO 3),
								dataa_memory_W  					=>dataa_memory_W,
								address_memory_W  				=>address_memory_W,
								----
								result_BASIC_UNIT_1      		=>result_UNIT_2);

---TANH FUNCTION
UNIT_1_TANH_1	: tanhvhdl port map(
								clk_tanh  							=>clk_LSTM_1,
								reset_tanh							=>reset_LSTM_1,
								enable_tanh	   					=>extra_enable_output_LSTM_2,
								input_tanh  						=>result_UNIT_2,
								result_1          				=>result_tanh_1);

---MEMORY-CELL-LSTM_1
-----------------------------------------------------------------------------------------------------------------------
UNIT_1_MEMORY_CELL_1 : MEMORY_CELL_1 port map(
								clk_MEMORY_CELL_1					=>clk_LSTM_1,
								reset_MEMORY_CELL_1  		 	=>reset_LSTM_1,
								enable_MEMORY_CELL_1 			=>enable_LSTM_1,
								enable_WRITE_MEMORY_CELL_1 	=>enable_write_memory_cell,
								data_INPUT_MEMORY_CELL_1 	 	=>result_adder_1_sec_LSTM_1,								
								result_MEMORY_CELL_1 			=>result_memory_cell_1);

---FORGET_GATE_1
-----------------------------------------------------------------------------------------------------------------------
UNIT_3_FORGET_GATE_1 : BASIC_UNIT_1 port map(
								clk_BASIC_UNIT_1					=>clk_LSTM_1,
								reset_BASIC_UNIT_1  		    	=>reset_LSTM_1,
								enable_BASIC_UNIT_1 				=>enable_LSTM_1,
								data_BASIC_UNIT_1_Wh 			=>data_INPUT_LSTM_Wh,
								data_BASIC_UNIT_1_Wx 			=>data_INPUT_LSTM_Wx,
								enable_OUTPUT_BASIC_UNIT_1		=>extra_enable_output_LSTM_3,
								--nios_ii 
								enable_write_memory				=>enable_write_memory,
								write_memory_select				=>write_memory_select(8 DOWNTO 6),
								dataa_memory_W  					=>dataa_memory_W,
								address_memory_W  				=>address_memory_W,
								----							
								result_BASIC_UNIT_1      		=>result_UNIT_3);
								
---SIGMOID FUNCTION
UNIT_1_SIGMOID_2	:	sigmoidvhdl port map(
								clk_sigmoid							=>clk_LSTM_1,
								reset_sigmoid						=>reset_LSTM_1,
								enable_sigmoid 					=>extra_enable_output_LSTM_3,
								input_sigmoid						=>result_UNIT_3,
								result_1      						=>result_sigmoid_2);
								
---OUTPUT_GATE_1
-----------------------------------------------------------------------------------------------------------------------
UNIT_4_OUTPUT_GATE_1 : BASIC_UNIT_1 port map(
								clk_BASIC_UNIT_1					=>clk_LSTM_1,
								reset_BASIC_UNIT_1  		    	=>reset_LSTM_1,
								enable_BASIC_UNIT_1 				=>enable_LSTM_1,
								data_BASIC_UNIT_1_Wh 			=>data_INPUT_LSTM_Wh,
								data_BASIC_UNIT_1_Wx 			=>data_INPUT_LSTM_Wx,
								enable_OUTPUT_BASIC_UNIT_1		=>extra_enable_output_LSTM_4,
								--nios_ii 
								enable_write_memory				=>enable_write_memory,
								write_memory_select				=>write_memory_select(11 DOWNTO 9),
								dataa_memory_W  					=>dataa_memory_W,
								address_memory_W  				=>address_memory_W,
								----							
								result_BASIC_UNIT_1      		=>result_UNIT_4);

---SIGMOID FUNCTION
UNIT_1_SIGMOID_3	:	sigmoidvhdl port map(
								clk_sigmoid							=>clk_LSTM_1,
								reset_sigmoid						=>reset_LSTM_1,
								enable_sigmoid 					=>extra_enable_output_LSTM_4,
								input_sigmoid						=>result_UNIT_4,
								result_1      						=>result_sigmoid_3);													
						
UNIT_1_UNIT_SEC_MULTI_1 : UNIT_SEC_MULTI_1 port map(
						clock											=>clk_LSTM_1,
						dataa											=>result_sigmoid_1,
						datab											=>result_tanh_1,
						result										=>result_multi_1);
				
UNIT_1_UNIT_SEC_MULTI_2 : UNIT_SEC_MULTI_1 port map(
						clock									=>clk_LSTM_1,
						dataa									=>result_sigmoid_2,
						datab									=>result_memory_cell_1,
						result								=>result_multi_2);
						

						
---ADDER-SECTION_1
UNIT_1_ADDER_SEC_1 : UNIT_SEC_ADDER_LSTM1 port map(
						clock									=>clk_LSTM_1,
						dataa									=>result_multi_1,
						datab									=>result_multi_2,
						result								=>result_adder_1_sec_LSTM_1_mod);
						

result_adder_1_sec_LSTM_1(14 DOWNTO 0) <= result_adder_1_sec_LSTM_1_mod(22 DOWNTO 8);
result_adder_1_sec_LSTM_1(15)<= result_adder_1_sec_LSTM_1_mod(31);	


---TANH FUNCTION
UNIT_1_TANH_LSTM_1 :tanhvhdl port map(
						clk_tanh  	      				=>clk_LSTM_1,
						reset_tanh							=>reset_LSTM_1,
						enable_tanh	   					=>enable_tanh_section,
						input_tanh  						=>result_adder_1_sec_LSTM_1,
						result_1 							=>result_tanh_sec_LSTM_1);
						
UNIT_1_UNIT_SEC_MULTI_3 : UNIT_SEC_MULTI_1 port map(
						clock									=>clk_LSTM_1,
						dataa									=>result_tanh_sec_LSTM_1,
						datab									=>result_sigmoid_3,
						result								=>result_multi_3);
						
output_multi_3_LSTM_1(14 DOWNTO 0) <= result_multi_3(22 DOWNTO 8);
output_multi_3_LSTM_1(15)<= result_multi_3(31);	
						


						
---HT_UNIT_1-LSTM_1
UNIT_1_STATE_HT    : HT_UNIT_1 port map(
						clk_HT_UNIT_1						=>clk_LSTM_1,
						reset_HT_UNIT_1    		    	=>reset_LSTM_1,
						enable_HT_UNIT_1 					=>enable_LSTM_1,
						enable_WRITE_HT_UNIT_1 			=>enable_HT_output_LSTM_1,
						data_INPUT_HT_UNIT_1 	 		=>output_multi_3_LSTM_1,
						result_HT_UNIT_1 				   =>data_INPUT_LSTM_Wh);

						
output_LSTM_1 					<= data_INPUT_LSTM_Wh;
enable_tanh_section			<= enable_write_memory_cell;

--enable OUTPUT-LSMT_1
process (clk_LSTM_1,enable_LSTM_1)
begin  
	if(clk_LSTM_1'event and clk_LSTM_1='1') then	
				if (reset_LSTM_1= '1') then
						counter_128_Ht_1 <= (OTHERS => '0');
						delay_Ht_1 <= (OTHERS => '0');
						enable_HT_output_LSTM_1 <= '0';
						counter_output_memory <= (OTHERS => '0');
				end if;
				if (enable_LSTM_1= '1') then
						if  (counter_output_memory =  "10011100") then --128(Neurons)+ x cycles of delay(MAC and MEMORY and ADDERS 1-2) 
									enable_HT_output_LSTM_1 <= '1';
									counter_output_memory <="00011000";  --6 INIT value to continue with the cycle of 128
									counter_128_Ht_1 <= counter_128_Ht_1 +1;
				
								
						else
									enable_HT_output_LSTM_1 <= '0';
									counter_output_memory <= counter_output_memory +1;
						
						end if;
				
				end if;
				
				if ( counter_128_Ht_1 > "01111111") then
				   	counter_output_memory <="00011000";
						if (delay_Ht_1 > "11000") then  				
									counter_128_Ht_1     <= (OTHERS => '0');
									delay_Ht_1 <= (OTHERS => '0');
							
						else 
									delay_Ht_1 <= delay_Ht_1 +1;
							
						end if;
					
				end if;
				
				
			
				
	end if;
end process;




--enable MEMORY-CELL/TANH_SEC
process (clk_LSTM_1,enable_LSTM_1)
begin  
	if(clk_LSTM_1'event and clk_LSTM_1='1') then	
	
				if (reset_LSTM_1= '1') then
						counter_128_Ht_2 <= (OTHERS => '0');
						delay_Ht_2 <= (OTHERS => '0');
						counter_memory_cell<= (OTHERS => '0');
						enable_write_memory_cell <= '0';
				end if;	
	
	
				if (enable_LSTM_1= '1') then
						if  (counter_memory_cell=  "10010110") then --128(Neurons)+ x cycles of delay(MAC and MEMORY and ADDERS 1-2) 
									enable_write_memory_cell <= '1';
									counter_memory_cell <="00010010";  --6 INIT value to continue with the cycle of 128
									counter_128_Ht_2 <= counter_128_Ht_2 +1;
				
								
						else
									enable_write_memory_cell <= '0';
									counter_memory_cell <= counter_memory_cell +1;
						
						end if;
				
				end if;
				
				if ( counter_128_Ht_2 > "01111111") then
						counter_memory_cell <="00010010";
						if (delay_Ht_2 > "11000") then  				
									counter_128_Ht_2 <= (OTHERS => '0');
									delay_Ht_2 <= (OTHERS => '0');
							
						else 
									delay_Ht_2 <= delay_Ht_2 +1;
							
						end if;
					
				end if;
				
			
				
	end if;
end process;







end Behavioral;
