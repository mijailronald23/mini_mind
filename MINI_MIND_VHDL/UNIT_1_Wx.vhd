library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

entity UNIT_1_Wx is
  port(
		clk_UNIT_1_Wx 			: IN STD_LOGIC  ;
		reset_Wx					: IN STD_LOGIC  ;
		enable_UNIT_1_Wx 		: IN STD_LOGIC  ;
		dataa_input_Wx   		: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		signal_ready_Wx 		: OUT STD_LOGIC  ;
		--nios_ii 
		enable_write_memory	: IN STD_LOGIC  ;
		write_memory_1			: IN STD_LOGIC  ;
		dataa_memory_Wx  		: IN STD_LOGIC_VECTOR (15	DOWNTO 0);
		address_memory_Wx 	: IN STD_LOGIC_VECTOR (8 DOWNTO 0);
		----
		result_Wx     		   : OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end UNIT_1_Wx;

architecture Behavioral of UNIT_1_Wx is 

--Mac component 16 bits (Signed)x 16 bits(Signed) 
component UNIT_MAC port(
		aclr3			: IN STD_LOGIC;
		clock0		: IN STD_LOGIC;
		dataa			: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		datab			: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		ena0			: IN STD_LOGIC ;
		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
end component;


--Matrix of Wx(128 x 128) for weights 
component UNIT_RAM_Wx port(
		aclr		   : IN STD_LOGIC  := '0';
		address		: IN STD_LOGIC_VECTOR (8 DOWNTO 0);
		clock			: IN STD_LOGIC  := '1';
		data			: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		rden			: IN STD_LOGIC  := '1';
		wren			: IN STD_LOGIC ;
		q				: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;


signal address_ram 			 : STD_LOGIC_VECTOR (8 DOWNTO 0)  :="111111111";
signal signal_ram1	 		 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal clear_1	 				 : STD_LOGIC  :='0';
signal mac_enable	 			 : STD_LOGIC  :='0';
signal read_memory_enable	 : STD_LOGIC  :='0';
signal counter		 			 : STD_LOGIC_VECTOR (2 DOWNTO 0)  :=(OTHERS => '0');
signal signal_ready_1 		 : STD_LOGIC  :='0';
signal counter_address      : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter3		 		 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter4		 		 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter5		 		 : STD_LOGIC_VECTOR (3 DOWNTO 0)  :=(OTHERS => '0');
signal counter6		 		 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal extra_mac_delay		 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal extra_mac_1 			 : STD_LOGIC_VECTOR (31 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_1 		 	 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_2 		 	 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_3 		 	 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal delay_Ht_4 		 	 : STD_LOGIC_VECTOR (4 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_1	 	 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_2	 	 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal counter_128_Ht_3	 	 : STD_LOGIC_VECTOR (7 DOWNTO 0)  :=(OTHERS => '0');
signal delay_reset  			 : STD_LOGIC_VECTOR (1 DOWNTO 0)  :=(OTHERS => '0');
signal reset_memory   	 	 : STD_LOGIC  :='0';
signal reset_memory_enable  : STD_LOGIC_VECTOR (1 DOWNTO 0)  :=(OTHERS => '0');


--nios ii variables 
signal data_input_memory	 : STD_LOGIC_VECTOR (15 DOWNTO 0)  :=(OTHERS => '0');
signal write_memory_value   : STD_LOGIC  :='0';

begin


-- UNIT MAC
MAC_Wx_1 : UNIT_MAC port map(
						aclr3		=>clear_1,
						clock0	=>clk_UNIT_1_Wx ,
						dataa    =>dataa_input_Wx,
						datab		=>signal_ram1,
						ena0		=>mac_enable,
						result	=>extra_mac_1 );
						
result_Wx (14 DOWNTO 0)<= extra_mac_1(22 DOWNTO 8); --Resize memory 
result_Wx (15)<= extra_mac_1(31);						 --Resize memory 

-- UNIT RAM 
RAM_Wx_1 : UNIT_RAM_Wx port map(
						aclr		=>reset_memory,
						address	=>address_ram,
						clock		=>clk_UNIT_1_Wx ,
						data		=>data_input_memory,
						rden		=>read_memory_enable,
						wren		=>write_memory_value,
						q			=>signal_ram1);					

-- UNIT - ENABLE - OUTPUT - FOR REGISTER
signal_ready_Wx <= signal_ready_1;

--process reset memory _ reg 
process (clk_UNIT_1_Wx)
begin  
if(clk_UNIT_1_Wx'event and clk_UNIT_1_Wx='1') then
	if (read_memory_enable = '1') then
			reset_memory <= '0';
				
	else
			reset_memory <= '1';
			
	end if;


end if;
end process;

-- process ram_mac 
process (clk_UNIT_1_Wx,enable_UNIT_1_Wx)
begin  
	if(clk_UNIT_1_Wx'event and clk_UNIT_1_Wx='1') then
			
			if (enable_write_memory = '1') then 				--write memory unit
					address_ram			<= address_memory_Wx;
					data_input_memory	<= dataa_memory_Wx;
					write_memory_value<= write_memory_1;
			end if; 
			if (reset_Wx = '1') then 								--reset all variables
					counter 				<=(OTHERS => '0');
					extra_mac_delay 	<= (OTHERS => '0');
					address_ram			<="111111111";
					delay_Ht_1 			<= (OTHERS => '0');
					counter_address   <= (OTHERS => '0');
					read_memory_enable<= '0';
			end if;
			
			if (enable_UNIT_1_Wx = '1') then 
					
					if ( counter> "011") then  						--read only 4 values of memory ram 
							if ( extra_mac_delay= "10000000") then  			--delay mac memory 128(size of net)
										counter <=(OTHERS => '0');
										extra_mac_delay <= (OTHERS => '0');
										counter_address <= counter_address +1;
										
							else	
										extra_mac_delay<= extra_mac_delay+ 1;
	
							end if;
					else	
	
							counter <= counter +1;
							address_ram <= address_ram +1;
							read_memory_enable <= '1';		
									
					end if;

						
			end if;		
			
			if (enable_UNIT_1_Wx = '0') then 
					read_memory_enable <= '0';
			end if;
				
			if ( counter_address > "01111111") then	   	--extra_mac_delaydelay cycle, necessary for save last values of HT(output) and CT(Cell-memory) 
					counter <=(OTHERS => '0');						--reset init counter 
					extra_mac_delay <= (OTHERS => '0');			--reset init extra_mac_delay
					address_ram <="111111111";						--reset address_ram
					if (delay_Ht_1 > "11000") then 	   		--extra_mac_delay cycle (25)
							delay_Ht_1 <= (OTHERS => '0');
							counter_address  <= (OTHERS => '0');
							
					else 
							delay_Ht_1 <= delay_Ht_1 +1;
							
					end if;
			end if;
					

	end if;

end process;


-----CLEAR MAC_value
process (clk_UNIT_1_Wx,enable_UNIT_1_Wx)
begin  
	if(clk_UNIT_1_Wx'event and clk_UNIT_1_Wx='1') then
				if (reset_Wx = '1') then 
					delay_Ht_2 <= (OTHERS => '0');
					counter_128_Ht_1 <= (OTHERS => '0');
					counter3 <=(OTHERS => '0');
					if (delay_reset = "11") then					-- delay for reset a register in Mac 
								clear_1 <= '0';
					else	
								clear_1 <= '1';
								delay_reset <= delay_reset +'1';
					end if;					
					
					
				end if;
	
				
				if (enable_UNIT_1_Wx = '1') then 				-- delay for reset a register in Mac , during matrix multiplication, 128 (size of the net) + 4 (Ram address) + 2 (Delay of Ram Configuration)
							delay_reset <= (OTHERS => '0');
							if ( counter3= "10000110") then
									clear_1 <= '1';
									counter3 <="00000010";			-- delay for reset a register in Mac , during matrix multiplication, 128 (size of the net) + 4 (Ram address)
									counter_128_Ht_1 <= counter_128_Ht_1+1;
							else	
									counter3 <= counter3 +1;
									clear_1 <= '0';	
							end if;
	
				end if;
				
				if (enable_UNIT_1_Wx = '0') then 
							counter3 <=(OTHERS => '0');
				end if;
				
				if (counter_128_Ht_1 > "01111111") then
							counter3 <="00000010";
							clear_1 <= '1';
							if (delay_Ht_2 > "11000") then
									delay_Ht_2 <= (OTHERS => '0');
									counter_128_Ht_1 <= (OTHERS => '0');
							else 
									delay_Ht_2 <= delay_Ht_2 +1;
							
							end if;
				end if;
				

	end if;
end process;

-----SIGNAL-READY
process (clk_UNIT_1_Wx,enable_UNIT_1_Wx)
begin  
	if(clk_UNIT_1_Wx'event and clk_UNIT_1_Wx='1') then
	
				if (reset_Wx = '1') then 
							counter4 <= (OTHERS => '0');
							delay_Ht_3 <= (OTHERS => '0');
							counter_128_Ht_2 <= (OTHERS => '0');
							signal_ready_1	<= '0';
				end if;
				if (enable_UNIT_1_Wx = '1') then 
		
							if ( counter4= "10000101") then
									signal_ready_1	<= '1';
									counter4 <="00000001";
									counter_128_Ht_2 <= counter_128_Ht_2 + 1;
							else	
									counter4 <= counter4 +1;
									signal_ready_1	<= '0';
									
							end if;
	
				end if;
				
				if (enable_UNIT_1_Wx = '0') then 
							counter4 <= (OTHERS => '0');
				end if;
				
				if (counter_128_Ht_2 > "01111111") then
							counter4 <="00000001";
							if (delay_Ht_3 > "11000") then
									delay_Ht_3 <= (OTHERS => '0');
									counter_128_Ht_2 <= (OTHERS => '0');
							
							else 
									delay_Ht_3 <= delay_Ht_3 +1;
							
							end if;
				end if;				
				

	end if;
end process;


-----MAC-ENABLE
process (clk_UNIT_1_Wx,enable_UNIT_1_Wx)
begin  
	if(clk_UNIT_1_Wx'event and clk_UNIT_1_Wx='1') then
	
				if (reset_Wx = '1') then 
							counter5 <= (OTHERS => '0');
							counter6 <=(OTHERS => '0');
							mac_enable	<= '0';
							delay_Ht_4 <= (OTHERS => '0');
							counter_128_Ht_3 <= (OTHERS => '0');				
				end if;
				if (enable_UNIT_1_Wx = '1') then 
		
							if ( counter5= "01001") then						--11 CYCLES ENABLE - MAC
									mac_enable	<= '0';
									if ( counter6= "01111011") then			--SYNCRONIZE DATA WITH A NEURON  123 (128- 5-DELAY TO INIT MAC AND RAM VALUES )
											
											counter5 <=(OTHERS => '0');
											counter6 <=(OTHERS => '0');
											counter_128_Ht_3 <= counter_128_Ht_3 + 1;
									else	
											counter6 <= counter6 +1;
	
									end if;
							else	
									mac_enable	<= '1';
									counter5 	<= counter5 +1;	
							end if;
	
				end if;
				
				if (enable_UNIT_1_Wx = '0') then 
							counter5 <= (OTHERS => '0');
							counter6 <=(OTHERS => '0');
				end if;
				
				if (counter_128_Ht_3 > "01111111") then
							mac_enable	<= '0';
							counter5 <=(OTHERS => '0');
							counter6 <=(OTHERS => '0');
							if (delay_Ht_4 > "11000") then
									delay_Ht_4 <= (OTHERS => '0');
									counter_128_Ht_3 <= (OTHERS => '0');
							
							else 
									delay_Ht_4 <= delay_Ht_4 +1;
							
							end if;
				end if;	

	end if;
end process;



end Behavioral;