LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;


entity sigmoidvhdl is               
  port(
	   clk_sigmoid	       : IN STD_LOGIC  ;
		reset_sigmoid	    : IN STD_LOGIC  ;
		enable_sigmoid 	 : IN STD_LOGIC  ;
		input_sigmoid		 : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result_1           : OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end sigmoidvhdl;

architecture Behavioral of sigmoidvhdl is 

--Logic comparator
component logic_comparator_sigmoid is               
  port(clk_comparator: IN STD_LOGIC  ;
		 dataa_input	: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		 result_1    	: OUT STD_LOGIC_VECTOR (1 DOWNTO 0));
end component;


--Logic adder
component adder_2 port
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result	: OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
end component;

--MAC
component multionly port (
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
end component;


signal logic_result : STD_LOGiC_VECTOR (1 DOWNTO 0):=(OTHERS => '0');
signal aux_a_line	  : STD_LOGIC_VECTOR (15 DOWNTO 0):=(OTHERS => '0');
signal aux_b_line	  : STD_LOGIC_VECTOR (15 DOWNTO 0):=(OTHERS => '0');
signal result_multi : STD_LOGIC_VECTOR (31 DOWNTO 0):=(OTHERS => '0');
signal mod_size_multi : STD_LOGIC_VECTOR (15 DOWNTO 0):=(OTHERS => '0');
signal result_multi_3 : STD_LOGIC_VECTOR (15 DOWNTO 0):=(OTHERS => '0');

begin

unitcomparator: logic_comparator_sigmoid port map (
									clk_comparator	=> clk_sigmoid,
									dataa_input	  	=> input_sigmoid,
									result_1     	=> logic_result);
													
unitsigmoid:  multionly  port map(

									clock  			=> clk_sigmoid,
									dataa	 			=> input_sigmoid,
									datab	 			=> aux_a_line,
									result 			=> result_multi);
																
result_multi_3(14 DOWNTO 0)<=result_multi(22 DOWNTO 8);
result_multi_3(15)<=result_multi(31);
unitadder: adder_2 port map(
									clock				=>clk_sigmoid,
									dataa				=>result_multi_3,
									datab				=>aux_b_line,
									result			=>result_1);
									
																	

process (clk_sigmoid,logic_result)
begin  
	if(clk_sigmoid'event and clk_sigmoid='1') then
		if(reset_sigmoid ='1') then
					aux_a_line<="0000000000000000"; 
					aux_b_line<="0000000000000000";						
		end if;
	

		if(enable_sigmoid ='1') then
			case logic_result is
					when "00" =>  
					aux_a_line<="0000000000000000"; --0
					aux_b_line<="0000000000000000"; --0
									 
					when "01" =>  
					aux_a_line<="0000000000110011"; --0.2
					aux_b_line<="0000000010000000"; --0.5
					
					when "10" =>  
					aux_a_line<="0000000000000000"; --0
					aux_b_line<="0000000100000000"; --1
					
					when others => 
					aux_a_line<="0000000000000000"; 
					aux_b_line<="0000000000000000";				
			end case;
		end if;
	end if;
end process;

end Behavioral;

